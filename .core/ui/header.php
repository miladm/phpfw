<?php
namespace ui;

/*
	- header structure
*/
class Header{
	// public $UIStyleFile = "style.css@main";
	// public $load_UIStyleFile = true;
	public $styleList = [];
	public $linkList = [];
	public $jsList = [];
	// public $jqFile = "system/jq";
	// public $load_jqFile = false;
	public $title = null;
	public $description = false;
	public $metaList = false;
	public $alternateList = false;
	public $faveIcon = "";
    public $mobile = true;
}