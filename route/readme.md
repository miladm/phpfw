[Home](../readme.md)

[bridge and conductor(old version)](./oldreadme.md)

# Router
Router is the request handler for framework. for now two types of request are supported
* GET
* POST

# Define routing
base on the request methods ```post``` or ```get``` you can choose from the methods below:
## GET
```php
route::get( $path, $callback );
```
## POST
```php
route::post( $path, $callback );
```

## add
this will got through both `post` and `get` request.

# path
Path can be both static and dynamic.

## static path
```
/sample_url/otherthing/other
```

## dynamic path
you can add paramteres (variables) to the url so you can use in the project. to define a parameter you have to add a ```:``` character to the begining of the url chunk. (url are chunked by slash "/")

```
/:name/:age
```
the parameters will be in the call back function as ```$request->param```.

### regex for dynamic path
you can add special reges for each parameter in pranteses.

```
/:name(\w)/:age(\d)
```
there are shortcode names for known regex as the table below:

| regex | shortcode |
| ---  | --- |
| integer numbers only | (number) |
| string only | (string) |
| string and numbers | (numberstring) |
| anything that can be in url | (url) |
| any regular expression you want | (<regex here>) |

**NOTE:** for your custom regex you should not add `/^` at the beginning and `$/` to the end of regex .. just the type you wanna match 

example:
```php
\Route::get('/:username([a-z]*)', function(){ ... });
```
# callback function
call back function needs two main parameters. first one is the request data and the second one is response prebuilt object.

```php

route::get( "/", function ( $request, $response ) {
    // codes here
} );
```

this function can be as variable or string too.

## requset parameter
the list below are the main data into ```$request``` parameter:
* get (data sent from get method header)
* post (data sent from post or php://input)
* file (file data sent from multipart form)
* param (parameters from dynamic url)


## response parameter
you can response the standard way using the ```$response``` paramtere's object. methods are as the list below:
* send( String ) : print string as response
* json ( Array, header = 200 ) : send json as response
    * header
        * default : 200 OK
        * 201 Created
        * 304 Not Modified
        * 400 Bad Request
* error( Header ) : response error header
* error404() : error 404 header for page not found
* error400() : error 400 header for bad request

# middleware(use)
there are types below for middleware
* add a global middleware
    ```php
    \Route::use(function($request, $response){
        ...
    });
    ```
* add middleware for some routes using regex
    ```php
    \Route::use('/someMatchesRoutes', function($request, $response){
        ...
    });
    // this will be used for /someMatchesRoutes/.*
    ```
* middleware for specific route
    ```php
    \Route::get('/myRoute/:param', function($req, $res) {
        ...
    })->use(function($request, $response){
        ...
    });
    ```
to make route pass the middleware `$response->next()` must be called.
```php
    \Route::use('/someMatchesRoutes', function($request, $response){
        if (something) {
            $response->next();
        } else {
            $response->send('there\'s error');
        }
    });
```

## attach data to next level
you can attach some data to next level using `$request->attach('parameter', 'value');`
```php
    \Route::use('/someMatchesRoutes', function($request, $response){
        if (something) {
            $request->attach('status', 'verry good');
            $response->next();
        } else {
            $response->send('there\'s error');
        }
    });
```

# then
adding functions to exact time before response handle. for example right after register done successfully send email;
```php
\Route::get('register', function(\route\Request $request, \route\Response $response) {
    //user registered
    $response->json([
        'status' => 'success',
        'userEmail' => 'a@mail.com'
    ]);
})
->use('someMiddleware here')
->then(function($response) {
    if ($response['status'] == 'success') {
        \Email::send($response['userEmail', 'welcome to system']);
    }
})
```
there can be multiple `then`. first `then` gets `$response` from the main route handler. other `then` will be feed `$response` from the return of the `then` before
```php
\Route::get('...', function($request, $response) {
    $response->send(5); //case $response is 5
})->then(function($response){
    if ($response > 1)
        return 'hello'
    else 
        return 'bye'
})->then(function($response){
    echo $response; // this response will be : 'hello'
    return false;
})->then(function($response){
    echo $response; // this response will be : 'false'
});
```

# tag
tag is the category name that the route will response to. for example the route is about to add a user, then the tag can be `user.add`.
```php
\Route::get(..)->use(..)->tag('testtag');
```
tag will be in `$response->getTag()` everywhere both in **middleware** and **route function**.
```php
function someMiddleware($request, $response)
{
    if ($request->tag == 'user.add')
        doSomething();
}
```

# update plan
* attach API system to router
* files routing
* return file url from the file shorthand url 
* define the router before start
    * setup the UI package to load all sheets of template
    * setup 404 and 400 error pages template (or any other default pages)
* response the template 
* redirect 
