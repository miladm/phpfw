<?php

namespace tool\jwt;

define('JWT_ENCODE_SALT', \Config::get('hash_id') ?: 'this is the basic hash and unsecure');

class JWT
{
    private $encoding_algorithm = 'sha256';
    private $key = JWT_ENCODE_SALT;
    private $payload = '';
    private $header = '';
    private $signature = null;
    private $timestamp = null;

    public function __construct($algorithm = false, $key = false)
    {
        if ($algorithm) {
            $this->encoding_algorithm = $algorithm;
        }
        if ($key) {
            $this->key = $key;
        }
        $this->timestamp = time();
    }

    public static function decodePayload($token)
    {
        $object = new static();
        $payload = explode(".", $token)[1];
        $data = json_decode($object->base64UrlDecode($payload));
        return $data;
    }

    public function create($payload): string
    {
        $payload = (object) $payload;
        $payload->__timestamp = $this->timestamp;
        $this->payload = json_encode($payload);
        $this->header = $this->generateHeader();
        $this->signature = $this->generateSignature();
        return implode('.', [
            $this->base64UrlEncode($this->header),
            $this->base64UrlEncode($this->payload),
            $this->base64UrlEncode($this->signature)
        ]);
    }

    public function verify($token)
    {
        if (strpos($token, '.') === false) {
            return false;
        }
        list($header, $payload, $signature) = explode('.', $token);
        $this->header = $this->base64UrlDecode($header);
        $header = json_decode($this->header);
        if ($header == null || $header->typ != 'JWT')
            return false;
        $this->payload = $this->base64UrlDecode($payload);
        $this->timestamp = json_decode($this->payload)->__timestamp ?? '';
        $this->signature = $this->base64UrlDecode($signature);
        return $this->signature == $this->generateSignature();
    }

    public function data()
    {
        return json_decode($this->payload);
    }

    private function generateHeader(): string
    {
        return json_encode([
            'alg' => $this->encoding_algorithm,
            'typ' => 'JWT'
        ]);
    }

    private function generateSignature(): string
    {
        $data = implode('.', [
            $this->base64UrlEncode($this->header),
            $this->base64UrlEncode($this->payload)
        ]);
        return hash_hmac(
            $this->encoding_algorithm,
            $data,
            $this->key . "_" . $this->timestamp
        );
    }

    private function base64UrlEncode(string $data): string
    {
        $urlSafeData = strtr(base64_encode($data), '+/', '-_');

        return rtrim($urlSafeData, '=');
    }

    private function base64UrlDecode(string $data): string
    {
        $urlUnsafeData = strtr($data, '-_', '+/');
        $paddedData = str_pad($urlUnsafeData, strlen($data) % 4, '=', STR_PAD_RIGHT);
        return base64_decode($paddedData);
    }
}
