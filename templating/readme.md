# create new templating
```php
class {{templating name}} extends \templating\Templating
{
    public function translateList()
    {
        return [
            "\{\{([^} ]*)\}\}" => "<?php .... ?>"
        ];
    }
}
```
# current templating

# extend new templating
1. create a class extends \templating\Templating
1. add `translateList` method. it must return an array of translate. we call it translate codex
    1. translate codex must have two regex check
        1. master reges that shows the container of the element like `/\{\{([$}]*)\}\}/`
        1. sub list must have the regex for matched item like `/^if (.*)$/`
        1. sub list value must be the string we want to replace like `<?php echo $1 ?>`
```php
class language extends \tremplating\Templating
{
    public function trasnlateList()
    {
        return [
            "/\{\{([^}]*)\}\}/" => [
                "/^([a-zA-Z][^ ]*)$/" => "<?php echo $1 ?? '' ?>",
                ....
            ]
        ]
    }
}
```
you can also create your own rendering method if it's different than the system _Templating_ module does.

```php
public function render(string $content)
{
    $translateList = $this->translateList();
    foreach ($translateList as $masterRegex => $subRegexList) {
        if (!preg_match_all($masterRegex, $content, $matchList))
        return $content;
        foreach ($matchList[1] as $index => $item) {
            $translation = $this->translate($item, $subRegexList);
            // if ($item != $translation) //<-- we commented this so it always replace
                $content = str_replace($matchList[0][$index], $translation, $content);
        }
    }
    return $content;
}
```

## php translate
| shortcode | translation |
| ---       | ---          |
| `${{...}}` | this will fource to be a php (backend) render |
| `${{mixedVariable}}` | `<?php echo $mixedVariable ?>` |
| `${{#mixedVariable}}` | `<?php if ($mixedVariable ?? false){ ?>` |
| `${{/mixedVariable}}` | `<?php } ?>` |
| `${{#mixedVariable as variable}}` | `<?php if ($mixedVariable ?? false) foreach($mixedVariable as $key => $variable) { ?>` |
| `${{/mixedVariable}}` | `<?php } ?>` |


## both side translate
this is to be possible to translate both side (frontend and backend)

| shortcode | translation |
| ---       | ---          |
| `{{...}}` | this will be rendered both in backend and frontend |
| `{{mixedVariable}}` | `<?php echo $mixedVariable ?>` |