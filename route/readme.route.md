[Home](../readme.md) > [conductor](readme.md)


# Add route
```php
route::get( $route, $function, $arguments = false );
```
```php
route::post( $route, $function, $arguments = false );
```
**NOTE:** adding route for ```get``` request method and ```post``` request method are the same and in this documentation we go through get but it's the same!
# `$route`

`$route` is the url that we are setting for the target function (or file). it can both be
**static** or **dynamic**.

### static
static means a certain word or number. you can also add routes
with sub-dir as much as you need.

**Note:** do **NOT** use names with extension.

examples for **static** routes:

| name | condition |
| ---- | ---- |
| `login` 		| good	|
| `script.js` 	| bad	|
| `user/login` 	| good	|
| `123/user` 	| good	|
| `user123` 	| good	|

### dynamic
dynamic addressing is a single segment (singel sub-dir part) of the url that should
follow a regular expression that is being set.

There are some default RegEx already on the conductor so you can choose between them
or you can setup your own RegEx directly from `route::get`

| label | regex | description |
| --- | --- | --- |
| `(real)` | `([0-9]*)` | Real numbers from `0` to `9` |
| `(alpha)` | `([a-zA-Z]*)` | English alphabets both uppercase and lowercase from `a` to `z` |
| `(realalpha)` | `([0-9a-zA-Z]*)` | English alphabets and numbers together both uppercase and lowecase |
| `(url)` | `([0-9a-zA-Z\_\-\%]*)` | This regex will supports all characters that will be supported as a url on browsers contains `_ - %` |

You can also add your custom regex directly into route. ex: `route::get("([abc]{,4})", ...);`
# `$function`
`$function` is the target function for the route. it can be both a function call name
or the function itself. as the main use of the function is to call the interface `php`
file there are some prepaired function int the framework.

| call name | description |
| --- | --- |
| load::build | to load the `php` file from `/build/{module or plugin}/i/` directory |
| load::tmp | to load the `tpl` file from `/build/{module or plugin}/t/` directory |
| load::core | to load the `php` file from `/core/` directory |

[read more about load class](../brain.md#load)

You can also call a function name as the target or build the funciton inside this parameter

Ex1 :
```php
function foo()
{
	echo "Hello World";
}

route::get( "hello", "foo" );
```

Ex2 :
```php
$foo = function () { echo "Hello World"; }

route::get( "hello", $foo );
```

Ex3 :
```php
route::get( "hello", function(){
	echo "Hello World";
} );
```

Example for more complicated functions :

```php
route::get( "hello", function(){
	if( $user->age > 18 )
		echo "hello";
	else
		echo "you don't have access to this page";
} );
```


# `$arguments`

`$arguments` are the data you wanna pass to target function. entering data are two types of
_array_ data or not a _array_ data. for multiple entries of the argument in the function you
need to add array entries.

Ex1:
```php
route::get("hello", function( $name ){...} , "danny");
```

Ex2:
```php
route::get("hello", function( $name, $age ){...} , ["danny", 25]);
```

Ex3 (extended):
```php
function foo( $name, $age )
{
	if( $age > 18 )
		echo "hello ". $name;
	else
		echo "sorry ".$name." you don't have access to this page! :(";
}

$data = [ "danny", 25 ];

route::get("hello", "foo", $data);
```
