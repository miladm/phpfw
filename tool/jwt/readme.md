# JWT
Jason Web Token is a method of authentication. [read More](https://jwt.io/introduction/)

# How to use
## generate new JWT
the main JWT requirement is the key that will be used in signature.

**NOTE:** the key is really Important and private and never must be published.

there's a main key as default in the jwt module but the primary key is the `hash_id` inside `fwconfig.json` file.

```php
$jwt_engine = new \tool\jwt\JWT();
$jwt_engine->create(['name' => 'foo', 'age' => 18]);
```
the output will be like
```
eyJhbGciOiJzaGEyNTYiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiZm9vIiwiYWdlIjoxOH0.a51c4be14803b1821c44266267fe325728347b38ff9f1f37bc681cd81af4de70
```

## validate JWT
```php
$jwt_engine = new \tool\jwt\JWT();
if ($jwt_engine->verify($token)) {
    ...
}
```
## get data
```php
$jwt_engine = new \tool\jwt\JWT();
if ($jwt_engine->verify($token)) {
    $data = $jwt_engine->data();
}
```