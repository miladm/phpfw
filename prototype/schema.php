<?php
namespace prototype;

/*
    global schema list to store pointer to all intigrated schemas
    - prevent from duplicating schema with the same name
    - load the schema as it needed
 */

$schemaNameList = [];

/*
    schema
 */
class Schema
{
    private $tableName;
    private $engine = "InnoDB";
    private $fieldList = [];
    private $publicFieldList = [];
    private $jsonFieldNameList = [];
    private $leftJoinList = [];
    // private $manyList = [];
    private $primaryKey = false;
    private $lastFieldPointer = null;

    function __construct($tableName, $defaultInit = true)
    {

        /*
            check schemaname has not be registered before if not register to general schema list
         */
        global $schemaNameList;
        if (!isset($schemaNameList[$tableName])) {
            $schemaNameList[$tableName] = &$this;
        }
        $this->tableName = $tableName;
        if ($defaultInit) $this->defaultInit();
    }

    /*
        setting engine on the init database
     */
    public function engine($engine)
    {
        $this->engine = $engine;
        return $this;
    }

    /*
        default init for now we have
            createTime
            updateTime
            id as the primary key
     */
    private function defaultInit()
    {
        $this->publicFieldList = [
            "createTime" => (object)["type" => "timestamp"],
            "updateTime" => (object)["type" => "timestamp"]
        ];

        //setting up the primarykey
        $this->primaryKey("id");
    }

    /*
        registering normal type of fields
     */
    private function &registerField($type, $name, $length, $notNull, $unique, $default)
    {
        $field = new DatabaseField();
        $field->type = $type;
        $field->name = $name;
        $field->length = $length;
        $field->notNull = $notNull;
        $field->unique = $unique;
        $field->default = $default;
        $this->fieldList[$name] = &$field;
        $this->lastFieldPointer = &$field;
        return $field;
    }

    /*
        register object type pf fields
     */
    private function &registerObject($name, $schemaName, $leftJoinOn, $notNull)
    {
        if (strpos($schemaName, "\\") !== false) {
            $namespace = substr(
                $schemaName,
                0,
                strrpos($schemaName, '\\') + 1
            );
            $schemaName = $schemaName::schemaName();
        }
        $field = new ObjectField();
        $field->type = "object";
        $field->name = $name;
        $field->schemaName = $schemaName;
        if (isset($namespace)) {
            $field->namespace = $namespace;
        }
        $field->leftJoinOn = $leftJoinOn;
        $field->notNull = $notNull;
        $this->fieldList[$name] = &$field;
        return $field;
    }

    /*
        check if target field has not been registered before
     */
    private function field_check($name)
    {
        if (in_array($name, ["createTime", "updateTime"]) || isset($this->fieldList[$name]))
            \Tool::error("invalid name for field " . $name);
    }

    /*
        get the list of leftJoin for this shcema on selection
     */
    public function leftJoinList()
    {
        return $this->leftJoinList;
    }

    /*
        set fields types
     */
    public function object($name, $schemaName, $leftJoinOn = "id", $notNull = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerObject($name, $schemaName, $leftJoinOn, $notNull);
        $this->publicFieldList[$name] = &$field;
        $this->lastFieldPointer = &$field;
        if (strpos($schemaName, "\\") !== false) {
            $schemaName = $schemaName::schemaName();
        }
        $this->leftJoinList[] = (object)["name" => $schemaName, "asName" => $name, "on" => $this->tableName . "." . $name . "=" . $name . ".id"];
        return $this;
    }

    public function private_object($name, $schemaName, $leftJoinOn = "id", $notNull = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerObject($name, $schemaName, $leftJoinOn, $notNull);
        $this->lastFieldPointer = &$field;
        if (strpos($schemaName, "\\") !== false) {
            $schemaName = substr(
                $schemaName,
                strrpos($schemaName, '\\') + 1
            );
        }
        $this->leftJoinList[] = (object)["name" => $schemaName, "asName" => $name, "on" => $this->tableName . "." . $name . "=" . $name . ".id"];
        return $this;
    }

    // public function many($name, $schemaName, $relationSchemaName = false, $nameOnSchema = false): Schema
    // {
    //     if (strpos($schemaName, "\\") !== false) {
    //         $namespace = substr(
    //             $schemaName,
    //             0,
    //             strrpos($schemaName, '\\') + 1
    //         );
    //         $schemaName = substr(
    //             $schemaName,
    //             strrpos($schemaName, '\\') + 1
    //         );
    //     }
    //     $this->manyList[$name] = [
    //         'schemaName' => $schemaName,
    //         'namespace' => $namespace ?? null,
    //         'relationSchemaName' => $relationSchemaName,
    //         'nameOnSchema' => $nameOnSchema
    //     ];
    //     return $this;
    // }

    // public function getManyList()
    // {
    //     return $this->manyList;
    // }

    public function primaryKey($name)
    {
        $this->field_check($name);
        if ($this->primaryKey) {
            $oldName = $this->primaryKey->name;
            unset($this->publicFieldList[$oldName]);
        }
        $field = new PrimaryKeyfield($name);
        $this->publicFieldList[$name] = &$field;
        $this->primaryKey = &$field;
        return $this;
    }

    public function int($name, $length = 11, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("int", $name, $length, $notNull, $unique, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function private_int($name, $length = 11, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("int", $name, $length, $notNull, $unique, false);
        return $this;
    }


    public function bigInt($name, $length = 11, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("bigInt", $name, $length, $notNull, $unique, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function string($name, $length = 200, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("string", $name, $length, $notNull, $unique, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function private_string($name, $length = 200, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("string", $name, $length, $notNull, $unique, false);
        return $this;
    }

    public function text($name, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("text", $name, 0, $notNull, $unique, false);
        $this->publicFieldList[$name] = $field;

        return $this;
    }

    public function private_text($name, $notNull = false, $unique = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("text", $name, 0, $notNull, $unique, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function boolean($name, $default = 0, $notNull = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("boolean", $name, 0, $notNull, false, $default);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function private_boolean($name, $default = 0, $notNull = false): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("boolean", $name, 0, $notNull, false, $default);
        return $this;
    }

    public function email($name): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("email", $name, 200, false, false, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function url($name): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("url", $name, 200, false, false, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function hash($name): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("hash", $name, 200, false, false, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }

    public function timestamp($name): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("timestamp", $name, 0, false, false, false);
        $this->publicFieldList[$name] = $field;
        return $this;
    }


    public function json($name): Schema
    {
        $this->field_check($name);
        $field = &$this->registerField("JSON", $name, 0, false, false, false);
        $this->publicFieldList[$name] = &$field;
        $this->jsonFieldNameList[] = $name;
        return $this;
    }

    public function getJsonList()
    {
        return count($this->jsonFieldNameList) ? $this->jsonFieldNameList : [];
    }

    /**
     * make current field private
     */
    public function private(): Schema
    {
        if (is_null($this->lastFieldPointer))
            return $this;
        $fieldName = $this->lastFieldPointer->name;
        unset($this->publicFieldList[$fieldName]);
        return $this;
    }

    /**
     * set current field as unique
     */
    public function unique(): Schema
    {
        if (is_null($this->lastFieldPointer))
            return $this;
        $this->lastFieldPointer->unique = true;
        return $this;
    }

    /**
     * set current field as notNull
     */
    public function notNull(): Schema
    {
        if (is_null($this->lastFieldPointer))
            return $this;
        $this->lastFieldPointer->notNull = true;
        return $this;
    }

    /**
     * set a default value for current field
     */
    public function default($value): Schema
    {
        if (is_null($this->lastFieldPointer))
            return $this;
        $this->lastFieldPointer->default = $value;
        return $this;
    }


    /**
     * set length for current field
     */
    public function length(int $value): Schema
    {
        if (is_null($this->lastFieldPointer))
            return $this;
        $this->lastFieldPointer->length = $value;
        return $this;
    }

    /**
     * insert data into schema template
     * validate data input
     *  trigger error on non validated
     * check for required fields
     *  trigger error on not set required fields
     */
    public function fetchData(array $data, bool $checkNotNull = true)
    {
        $returnData = [];
        foreach ($this->fieldList as $fieldName => $template) {
            // check if input is required and isset in data
            if ($template->notNull && $template->default === false && $checkNotNull) {
                if (!isset($data[$fieldName])) {
                    return $this->error('fetchData', "$fieldName required but has not been set in data");
                }
            }
            if (isset($data[$fieldName]) && !is_null($data[$fieldName])) {
                $value = $data[$fieldName];

                // validate datatype
                $validationResult = \prototype\Validate::check($value, $template->type);
                if (!$validationResult)
                    return false;

                // if everything is alright then push to return data array
                $returnData["$this->tableName.$fieldName"] = $this->cookData($data[$fieldName], $template->type);
            }
        }

        // if there's any id entered then take care of it
        if (isset($data['id']) && is_int($data['id'])) {
            $returnData['id'] = $data['id'];
        }
        return $returnData;
    }

    /*
        return the schema and mapping to scheme the result rows
        note it goes only one level inside object connections
     */
    public function fetchSchema($asName = false, $preventObjects = false)
    {
        global $schemaNameList;
        foreach ($this->publicFieldList as $name => $field) {
            if ($field->type == "object" && !$preventObjects) {
                $fetchSchema[$name] = (object)$schemaNameList[$field->schemaName]->fetchSchema($field->name, true);
            } else {
                if ($asName)
                    $fetchSchema[$name] = "schema_" . $asName . "_" . $name;
                else
                    $fetchSchema[$name] = $name;
            }
        }
        return $fetchSchema;
    }

    /**
     * this will fetch data but it will make one object as alignment
     * so it will have better schema architecture on many to many relations 
     */
    // public function alignedFetchSchema($alignmentObjectName)
    // {
    //     die(var_dump(
    //         $alignmentObjectName
    //     ));
    //     global $schemaNameList;
    //     $fetchSchema = $schemaNameList[$alignmentObjectName]->fetchSchema($alignmentObjectName, true);
    //     foreach ($this->publicFieldList as $name => $field) {
    //         if ($name == $alignmentObjectName)
    //             continue;
    //         if (isset($fetchSchema[$name]))
    //             continue;
    //         if ($field->type == "object" && !$preventObjects) {
    //             $fetchSchema[$name] = $schemaNameList[$field->schemaName]->fetchSchema($field->name, true);
    //         }
    //         else
    //             if ($asName)
    //                 $fetchSchema[$name] = "schema_" . $asName . "_" . $name;
    //             else
    //                 $fetchSchema[$name] = $name;
    //     }
    //     return $fetchSchema;
    // }

    /*
        list of the public keys and select list of objects
        note it goes only one level inside object connections
     */
    public function selectList($asName = false, $preventObjects = false)
    {
        global $schemaNameList;
        foreach ($this->publicFieldList as $name => $field) {
            if ($field->type == "object" && !$preventObjects) {
                if (!isset($schemaNameList[$field->schemaName])) {
                    if (!is_null($field->namespace)) {
                        if (class_exists(strtolower($field->namespace) . $field->schemaName)) {
                            call_user_func(strtolower($field->namespace) . $field->schemaName . '::start');
                        }
                    } elseif (class_exists($field->schemaName)) {
                        call_user_func($field->schemaName . '::start');
                    } elseif (class_exists('\\' . strtolower($field->schemaName) . '\\' . $field->schemaName)) {
                        call_user_func('\\' . strtolower($field->schemaName) . '\\' . $field->schemaName . '::start');
                    } else {
                        \Tool::error("the schema name $field->schemaName is invalid!");
                    }
                }
                foreach ($schemaNameList[$field->schemaName]->selectList($field->name, true) as $value)
                    $selectList[] = $value;

                // updating jsonName list
                $objectJsonNameList = $schemaNameList[$field->schemaName]->getJsonList();
                if ($objectJsonNameList) {
                    foreach ($objectJsonNameList as $value)
                        $this->jsonFieldNameList[] = "schema_" . $field->name . "_" . $value;
                }
            } else {
                if ($asName)
                    $selectList[] = $asName . "." . $name . " as " . "schema_" . $asName . "_" . $name;
                else
                    $selectList[] = $this->tableName . "." . $name;
            }
        }
        return $selectList;
    }

    /*
        query to be runned on creating database on the firs run (init mode run)
     */
    public function init_query()
    {
        return "
        create table if not exists `$this->tableName` (
            " . $this->init_query_keys() . "
            `createTime` timestamp not null default current_timestamp,
            `updateTime` timestamp default current_timestamp ON UPDATE CURRENT_TIMESTAMP,
            " . $this->init_query_fields() . "
            " . $this->init_query_keyConfigurations() . "
       )engine=$this->engine default charset = utf8 collate=utf8_general_ci auto_increment=1;
        ";
    }

    private function init_query_keys()
    {
        return "`" . $this->primaryKey->name . "` int(" . $this->primaryKey->length . ") not null auto_increment,";
    }

    private function init_query_keyConfigurations()
    {
        return "primary key(`" . $this->primaryKey->name . "`)";
    }

    private function init_query_fields()
    {
        $string = "";
        foreach ($this->fieldList as $name => $schema)
            $string .= $this->init_query_field_string($schema);
        return $string;
    }


    private function init_query_field_string($schema)
    {

        //name
        $r = "`$schema->name`";
        $r .= " ";

        //type and length
        switch ($schema->type) {
            case "int":
                $r .= "int($schema->length)";
                break;
            case "string":
            case "email":
            case "url":
            case "hash":
                $r .= "varchar($schema->length)";
                break;
            case "object":
                $r .= "int(" . $this->primaryKey->length . ")";
                break;
            case 'JSON':
                $r .= 'text';
                break;
            default:
                $r .= $schema->type;
        }
        $r .= " ";

        //not null
        if ($schema->notNull) {
            $r .= "not null";
            $r .= " ";
        }

        //unique
        if ($schema->unique) {
            $r .= "unique";
            $r .= " ";
        }
        if ($schema->default !== false) {
            if ($schema->default === 'CURRENT_TIMESTAMP') {
                $r .= "default " . $schema->default . "";
            } else {
                $r .= "default '" . $schema->default . "'";
            }
            $r .= " ";
        }
        $r .= ",
            ";
        return $r;
    }

    /**
     * get data and the type and do anything on data if necessary
     */
    private function cookData($data, $dataType)
    {
        switch ($dataType) {
            case 'hash':
                return \Tool::hash($data);
            case 'string':
                return (string)$data;
            case 'object':
                if (is_int($data)) {
                    return $data;
                } elseif (is_string($data)) {
                    return (int)$data;
                } else {
                    return $data->id;
                }
            case 'boolean':
                return $data ? 1 : 0;
            case 'JSON':
                return json_encode($data);

            default:
                return $data;
        }
    }

    public function getName()
    {
        return $this->tableName;
    }

    public function getKey()
    {
        return $this->primaryKey->name;
    }

    private function error($parameter, $value)
    {
        \_E::set($parameter, $value);
        return false;
    }
}
