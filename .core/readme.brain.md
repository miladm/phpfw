[Home](readme.md)

<!-- # Brain 🧠
Brain ([/core/brain.php 📁](brain.php)) is the place everything comes together. hooks are loading here and the vital funcitons and class will be defined here.

To make configurations happen brain included _/set/configs.php_. configs file must be as below :

```php
class conf
{ 
	protected $connection = ["host" => ..., "name" => ..., "user" => ..., "pass" => ...];
	protected $memcache = ["activation" => ..., "host" => ..., "port" => ...];
	protected $conf = [ .... ];
}
```
[Read more about configs.php](#)

This _conf_ class will be the parent of the most important class of this framework. the **brain** class.

## brain class
This class contains the most important functions and datas (configs) of the project. **Each class that comes into project, must extends this class so they can have all the data and functions.**

### Data
As this class extends configs, all configs will come to brain too.so anywhere on the project you can call `$this->conf['...']` to get data and on the other hand you can add your modules' configs to the _configs.php_ so it becomes easier to control.

### Functions
| code                   	| 	description		|
| ---						|	-------			|
| `$this->url()`	 		| get current websites url that configed into configs.php |
|	`$this->opt()`   		| clears all conf data from the current object and returns the colne object with no configs data |
|	`$this->_e()`			| to set any error in any part of project. --- must add document ---|


## Defines and Functions out of brain class
There are some **defines** out of the brain class so it will be accessable everywhere on the project

### Defines

| Name								| description |
| ---- 								| --- |
| `$brain` (global access only!)    | this is an **object** of brain to access as a global variable. |
| `__url` 							| returns the current url |

### Functions
| Name 								| description |
| ---- 								| --- |
| `red( $url )` | to redirect between pages. `$url` must be the target without the website url. ex: `red('a')` goes to `_sitename_/a` |
| `plugin($name)` | it will setup modules and plugins that you made, just by calling the names |
| `panelAutorized()` | function to set for verifying user's access to panel |
| `SCRIPTERROR( $value )` | to trigger any known error in any part of project |
| `furl( $file )` | turns magicURL to the know url of the current website |

### Language 🌏
To manage multi-langage project

| Name | description |
| --- | ---- |
| `lang::set( $languageName )` | will define the target language name to rest of compile |
| `lang::get()` | returns the current language |

### Load
To load any type of known file from anywhere in the project. you can even use _magicURL_ to access easier.

| Name | description |
| --- | ---- |
| `load::build( $file[, $data] )` | loads **.php** file from _/build_ dir. that contains plugiins and modules |
| `load::core( $file[, $data] )` | loads **.php** file from _/core_ dir. that contains hooks of this framework. |
| `load::tmp( $file[, $data] )` | loads **.tpl** file from _/build/[plugin]/t_ dir. template files must be in the _t_ directory of each Plugin |
| `load::readFile( $file )` | loads **any type** of file from _/build/[plugin]/f_ dir. It contains image files, CSS and javascript files but there's no limitations on extension. |
 -->
