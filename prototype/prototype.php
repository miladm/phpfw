<?php

/*
    prototype is the pre-built data-access layer
 */
//beacase it's the data-acess layer and it will implement table in itself
abstract class Prototype
{
    /**
     * @var \prototype\IPrototype
     */
    public $IPrototype = null;

    function __construct()
    {
        if (!method_exists($this, "init"))
            \Tool::error("prototype must have an 'init' method");
        $this->IPrototype = new \prototype\IPrototype();
        $this->init();
        $this->setupTable();
    }

    abstract protected function init();

    /**
     * @return \prototype\IPrototype
     */
    public static function IPrototype()
    {
        return (new static())->IPrototype;
    }

    /**
     * @return \prototype\IPrototype
     */
    // public static function where($condition)
    // {
    //     return self::IPrototype()->where($condition);
    // }

    /**
     * @return \prototype\IPrototype
     */
    public static function groupBy($column)
    {
        return self::IPrototype()->groupBy($column);
    }

    /*
        prototype methods are ....
     */
    public static function create()
    {
        $return = self::IPrototype()->create();
        echo 'initiating ' . get_called_class() . PHP_EOL;
        if (method_exists(get_called_class(), 'onCreate')) {
            get_called_class()::onCreate();
        }
        return $return;
    }

    /**
     * @return Prototype
     */
    public static function start() : Prototype
    {
        return new static();
    }

    public static function add(array $data)
    {
        return self::IPrototype()->add($data);
    }

    public static function update(array $newData, array $condition)
    {
        return self::IPrototype()->update($newData, $condition);
    }

    public static function map(array $map) : \prototype\IPrototype
    {
        return self::IPrototype()->map($map);
    }

    public static function get(array $condition = [], $limit = false, $start = false, $asc = true)
    {
        return self::IPrototype()->get($condition, $limit, $start, $asc);
    }

    public static function getById(int $id)
    {
        return self::IPrototype()->getById($id);
    }

    public static function getOne(array $condition = [], $asc = true)
    {
        return self::IPrototype()->getOne($condition, $asc);
    }

    public static function getFirst(array $condition = [])
    {
        return self::IPrototype()->getFirst($condition);
    }

    public static function getLast(array $condition = [])
    {
        return self::IPrototype()->getLast($condition);
    }

    public static function count(array $condition = [])
    {
        return self::IPrototype()->count($condition);
    }

    public static function sum(array $condition = [], $column)
    {
        return self::IPrototype()->sum($condition, $column);
    }

    public static function expect(array $condition = [], $number = 1)
    {
        return self::IPrototype()->expect($condition, $number);
    }

    public static function delete(array $condition = [])
    {
        return self::IPrototype()->delete($condition);
    }

    public static function schemaName()
    {
        return self::IPrototype()->schemaName();
    }

    /**
     *  @return \prototype\Schema
     */
    public function schema($name = false)
    {
        return $this->IPrototype->schema($name);
    }

    private function setupTable()
    {
        $this->IPrototype->setupTable();
    }
}
