<?php

\Config::setAll([
	"index" => false,
	"database_connection" => ["host" => "miladm-mariadb", "name" => "test", "username" => "a", "password" => "a"] ,
	"url" => "http://localhost",
	'company' => 'company name',
	'email' => 'noreply@localhost.com',
	'domain' => 'localhost',
	"hash_id" => 'salts',
	"timezone" => "asia/tehran",
	// "plugin_template_dir" => "t",
	// "plugin_template_default_extension" => ".htm",
	"plugin_file_dir" => "f",
	"autoload_controller" => true
]);

// \Config::setAll([
// 	'elasticemail_api_url' => 'https://api.elasticemail.com/v2',
// 	'elasticemail_api_key' => 'your_elastic_api_key'
// ]);
