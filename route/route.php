<?php
$globalRouter = new \route\Router();

class Route
{
	public static function use($routePattern, callable $middleware = null): \route\Router
	{
		$router = self::getRouter();
		return $router->use($routePattern, $middleware);

	}

	public static function get($path, callable $function, $arguments = false): \route\Response
	{
		$router = self::getRouter();
		return $router->get($path, $function, $arguments);
	}

	public static function post($path, callable $function, $arguments = false): \route\Response
	{
		$router = self::getRouter();
		return $router->post($path, $function, $arguments);
	}

	/**
	 * (BUG)issue on setting tag to add.
	 * it adds both get and post but when it comes to tag it only assign the post
	 * so with the get method there's no tag
	 */
	// public static function add($path, callable $function, $arguments = false)
	// {
	// 	$router = self::getRouter();
	// 	return $router->add($path, $function, $arguments);
	// }

	public static function start($autoLoaderFunction = false)
	{
		$router = self::getRouter();
		$router->start($autoLoaderFunction);
	}

	public static function isFree($path)
	{
		$router = self::getRouter();
		return $router->isFree($path);
	}

	public static function dump()
	{
		$router = self::getRouter();
		return $router->getRouteList();
	}

    public static function getRouter(): \route\Router
    {
        global $globalRouter;
        if(!$globalRouter)
            $globalRouter = new \route\Router();
        return $globalRouter;
    }
	// public static function getFileUrl($file)
	// {
	// 	$file = load::cookFileAdress($file, "f");

	// 	global $con;
	// 	return ($con->getRule($file))? __url.$con->getRule($file) : false;
	// }

	// public static function fileUrl($file)
	// {
	// 	echo ($r = bridge::getFileUrl($file))? $r : SCRIPTERROR("ERROR 404 : the bridge for file ('".$file."') has not set!") ;
	// }
}