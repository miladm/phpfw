<?php
namespace tool;

define("LOCAL_EMAIL_LOGGER", \Config::get("email")["local_email"] ?? false);
define("LOCAL_EMAIL_LOG_FILE", \Config::get("email")["file"] ?? "file/localEmail.log");

class Email
{
	private $headerList = false;
	private $from = false;
	private $toList = false;
	private $subject = false;
	private $message = false;
	private $htmlTemplate = false;
	private $replyTo = false;
	private $cc = false;
	private $bcc = false;

	public function from($email, $name = false) :Email
	{
		$this->from = ["email" => $email, "name" => $name];
		return $this;
	}
	
	public function replyTo($email, $name = false) :Email
	{
		$this->replyTo = ["email" => $email, "name" => $name];
		return $this;
	}

	public function to($email, $name = false) :Email
	{
		if (is_array($email))
			if (!$this->toList) 
				$this->toList = $email;
			else 
				$this->toList = array_merge($this->toList, $email);
		else
			$this->toList[] = $email;
		return $this;
	}

	public function htmlHeader($template = false) :Email
	{
		$this->headerList[] = "MIME-Version: 1.0";
		$this->headerList[] = "Content-Type: text/html; charset=UTF-8";
		if ($template) {
			$file = "build/" . \Tool::cookFileAdress($template, 't') . ".tpl";
			if (!is_file($file)) Tool::error("file : " . $file . " not exists~!");
			$this->htmlTemplate = file_get_contents($file);
		} 
		return $this;
	}

	public function message($subject, $message) :Email
	{
		$this->subject = $subject;
		$this->message = $message;
		if ($this->htmlTemplate && is_array($this->message)) {
			$messageContent = $this->htmlTemplate;
			foreach($this->message as $schema => $replace)
				$messageContent = str_replace($schema, $replace, $messageContent);
			$this->message = $messageContent;
		}
		return $this;
	}

	public function send()
	{
		if (!$this->toList) return false;
		$to = implode(",", $this->toList);
		if (LOCAL_EMAIL_LOGGER) 
			return $this->localMail($to, $this->subject, $this->message, $this->generateHeader());
		else 
			return mail($to, $this->subject, $this->message, $this->generateHeader());
	}

	private function generateHeader()
	{
		#from
		if ($this->from) {
			if ($this->from["name"]) 
				$this->headerList[] = 'From: ' . $this->from["name"] . "<" . $this->from["email"] . ">";
			else 
				$this->headerList[] = 'From: ' . $this->from["email"];
		}else
			$this->headerList[] = 'From: ' . $this->conf["email"];

		#reply to
		if ($this->replyTo) {
			if ($this->replyTo["name"]) 
				$this->headerList[] = 'Reply-To: ' . $this->replyTo["name"] . "<" . $this->replyTo["email"] . ">";
			else 
				$this->headerList[] = 'Reply-To: ' . $this->replyTo["email"];
		}
		return implode(PHP_EOL, $this->headerList);
	}

	private function localMail($to , $subject, $message, $header)
	{
		$preData = false;
		if (file_exists(LOCAL_EMAIL_LOG_FILE))
			$preData = (array) json_decode(file_get_contents(LOCAL_EMAIL_LOG_FILE));
		$preData[] = [
			"to" => $to,
			"subject" => $subject,
			"message" => $message,
			"header" => $header
		];
		\Tool::force_file_put_contents(LOCAL_EMAIL_LOG_FILE, json_encode($preData));
	}
}