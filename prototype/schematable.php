<?php
namespace prototype;

/*
    create a table class from schema to be used by data-access layer
*/
class SchemaTable extends \Table
{
    public $name;

    function __construct( \prototype\Schema $schema )
    {
        $this->schema = $schema;
        $this->name = $this->schema->getName();
        $this->key  = $this->schema->getKey();
        parent::__construct();
    }
}
