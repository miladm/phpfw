<?php

use Superbalist\PubSub\Redis\RedisPubSubAdapter;
use Predis\Client;

class Redis
{
    private $connectionArray = [];
    private $client = null;
    private $adapter = null;

    function __construct(\redis\Connection $connectionData)
    {
        $this->connectionArray = [
            'scheme' => $connectionData->scheme,
            'host' => $connectionData->host,
            'port' => $connectionData->port,
            'database' => $connectionData->database,
            'read_write_timeout' => $connectionData->read_write_timeout
        ];
        $this->client = new Client($this->connectionArray);
        $this->adapter = new RedisPubSubAdapter($this->client);
    }

    public function subscribe($channelName, callable $callback)
    {
        $this->adapter->subscribe($channelName, $callback);
    }

    public function publish($channelName, $data){
        $encodedData = base64_encode($data);
        return $this->adapter->publish($channelName, $encodedData);
    }

    public function pubSub($channelName, $data, $replyChannelName, $callback){
        $data = json_encode((object)[
            "replyChannelName" => $replyChannelName,
            "data" => $data
        ]);
        $this->publish($channelName, $data);
        $this->subscribe($replyChannelName, $callback);
    }
}
