<?php
/**
 * verison 1 : stable
 */

define('prod', false);
define('APACHE', false);
include 'autoloader.php';

// project defines
define("__url", \Config::get("url"));
if( $timezone = \Config::get( "timezone" ) )
	date_default_timezone_set( $timezone );

// start OB and session to rull all over the project
ob_start();
session_start();
// set default route

include 'zzz/app.php';

// respond to the current request.
// if route not available then call the file autoloader check
\route::start("\\Tool::routeAutoLoadCheck");