# Core ⚛
----about core-----
## Brain 🧠
Brain ([/core/brain.php 📁](brain.php)) is the place everything comes together. hooks are loading here and the vital funcitons and class will be defined here.
[Read More ...](brain.md)
<!-- 
## Hooks

|	Name								|	Description							|
|	-----								|	----------							|
|	[action](action/readme.md)	 			|	setup label in parts of code then add actions later without editing plugns |
|	[API](apis/readme.md)	 			|	framework to build and manage API for script using RESTFul web service	|
|	[conductor](conductor/readme.md)	|	Setting urls and handling bridges	|
|	[database](database/readme.md)	 	|	communication with database	| -->