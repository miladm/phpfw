<?php

// use tool\FileExplorer;

class AutoLoader
{
	static $indexList = [SOURCE_DIR, FW_DIR];

	public static function load($calledClass)
	{
		// cook class name to a knowable class name
		$className = ltrim($calledClass, '\\');
		/* 
			check for it in src if it's not there then check in core
		*/

		// namespace rto directory of target file
		$lastNamespace = "";
		if ($lastNsPos = strrpos($className, '\\')) {
			$namespace = substr($className, 0, $lastNsPos);
			$className = substr($className, $lastNsPos + 1);
			$lastNamespace  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
		}
		$file =  strtolower($lastNamespace . $className . '.php');
		$mainDirList = static::$indexList;
		foreach ($mainDirList as $mainDir) {
			$mainDir = $mainDir . DIRECTORY_SEPARATOR;
			if (file_exists($mainDir . $file))
				require_once $mainDir . $file;
			if ((!file_exists($mainDir . $file) || !class_exists($calledClass))) { // && $lastNamespace == "") {
				$conditionList = [
					$mainDir . $lastNamespace . $className . ".php",
					$mainDir . $lastNamespace . strtolower($className) . DIRECTORY_SEPARATOR . $className . ".php",
					$mainDir . $lastNamespace . $className . DIRECTORY_SEPARATOR . $className . ".php",
					$mainDir . $lastNamespace . strtolower($className) . DIRECTORY_SEPARATOR . strtolower($className) . ".php"
				];
				foreach ($conditionList as $filePath) {
					if (!file_exists($filePath)) {
						continue;
					}
					require $filePath;
					if (!class_exists($calledClass)) {
						continue;
					} else {
						break;
					}
				}
			}
			if (class_exists($calledClass)) {
				break;
			}
		}
	}
}

\spl_autoload_register('AutoLoader::load');

if (file_exists('../../autoload.php')) {
	require_once '../../autoload.php';
} elseif (file_exists('vendor/autoload.php')) {
	require_once 'vendor/autoload.php';
}
