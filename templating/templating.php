<?php
namespace templating;

abstract class Templating
{
    public function render(string $content)
    {
        $translateList = $this->translateList();
        foreach ($translateList as $masterRegex => $subRegexList) {
            if (!preg_match_all($masterRegex, $content, $matchList))
            return $content;
            foreach ($matchList[1] as $index => $item) {
                $translation = $this->translate($item, $subRegexList);
                if($translation != $item)
                    $content = str_replace($matchList[0][$index], $translation, $content);
            }
        }
        return $content;
    }

    protected function translate($item, $translateList)
    {
        foreach ($translateList as $pattern => $replace) {
            if ($pattern[0] != "/")
                $pattern = "/^" . $pattern . "$/";
            $item = preg_replace($pattern, $replace, $item);
        }
        return $item;
    }

    abstract public function translateList();
}