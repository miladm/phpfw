<?php
/*
Production name: actions controller
Version: 2.0.0
Author: Milad Mohebnia
Modify date: 27/04/2018
Description: 
	This script helps you set action lists and  do things after or before the action execute but you don't wanna edit the main source and risk bugs!
	for example you have action named LOGIN and you want to say 'HI' right before start the login and maybe 
	a redirect after login
Documentation:

*/
class Action extends \globalstore\Globalstore
{
	public static function add($actionName, $callback)
	{
		$actionList = self::get($actionName);
		if(!$actionList)
			$actionList = [];
		$actionList[] = $callback;
		self::set($actionName, $actionList);
	}

	public static function run($actionName, $data)
	{
		foreach (self::get($actionName) as $callback)
			if(is_callable($callback))
				call_user_func($callback, $data);
	}
}