<?php

/*
	- ui is a calss to control and handle user interface better
		- better managing html
		- standard the preview HTML
		- merge styles and javascripts
		- make easier UI pages from templates ans samples
		- custom UI design and developable ui
*/
namespace ui;

define("SITENAME", \Config::get("sitename") ?: "");
define("UI_FILE_CACHE", \Config::get("ui_file_cache"));
define("TEMPLATE_EXTENSION", \Config::get("template_extension") ?: ".htm");
define("QUEUE_LOAD_ELEMENT", "<loadQ>");

class UserInterface
{
	private $header = null;
	private $loadQueue = false;
	private $Qdata = [];
	private $module = false;
	private $fileCache = null;
	private $middleware = [];

	function __construct(){
		$this->fileCache = new \tool\Filecache("UI");
		if (is_null($this->header))
			$this->header = new Header;
		$this->setTitle(SITENAME);
	}

	function __clone()
	{
		$this->header = clone $this->header;
	}

	public function meta($property, $content)
	{
		$this->header->metaList[$property] = $content;
		return $this;
	}

	public function alternate($href, $lang)
	{
		$this->header->alternateList[$href] = $lang;
		return $this;
	}

	public function mobile($set = true)
	{
		$this->header->mobile = $set;
		return $this;
	}

	public function style($file , $load = true, $version = false)
	{
		$this->header->styleList[$file] = [
			"file" =>	$file,
			"load" =>	$load,
			"version" => $version
		];
		return $this;
	}

	public function js($file, $load = true, $version = false, $header = false)
	{
		$this->header->jsList[$file] = [
			"file" =>	$file,
			"load" =>	$load,
			"header" => $header,
			"version" => $version
		];
		return $this;
	}
	public function setTitle($title)
	{
		$this->header->title = $title;
		return $this;
	}

	public function getTitle()
	{
		return $this->header->title;
	}

	public function title($title)
	{
		$this->header->title .= "-" . $title;
		return $this;
	}
		
	public function description($description)
	{
		$this->header->description = $description;
		return $this;
	}

	public function addQ($file, $data = false)
	{
		$this->loadQueue[] = $file;
		if ($data) 
			$this->Qdata = array_merge($data, $this->Qdata);
		return $this;
	}

	public function render($template, $data = [])
	{
		$data = array_merge($data, $this->Qdata);
		$page_content = $this->build_header();
		$page_content .= $this->build_body($template);
	
		$headerData = (object)[
			"title" => $this->getTitle(),
			"elements" => $this->header_write()
		];
		$page_content = $this->runMiddleware($page_content);
		if (isset($data)) 
			extract($data);
		eval('?>' . $page_content);
		return $this;
	}

	public function middleware(callable $MW)
	{
		$this->middleware[] = $MW;
		return $this;
	}

	private function build_header()
	{
		$header = '<!DOCTYPE html>'. PHP_EOL;
		$header .= '<html>'. PHP_EOL;
		$header .= '	<head>'. PHP_EOL;
		$header .= '	<title><?php echo $headerData->title ?></title>'. PHP_EOL;
		$header .= '		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">'. PHP_EOL;
		$header .= '		<link rel="icon" href="/favicon.ico" type="image/x-icon">'. PHP_EOL;
		$header .= '		<?php echo $headerData->elements ?>'. PHP_EOL;
		$header .= '	</head>'. PHP_EOL;
		$header .= '	<body>'. PHP_EOL;
		return $header;
	}

	private function header_write()
	{
		$headerElements = "";

		#setting up description
		if (!$this->header->description) 
			$this->description($this->header->title);
		$headerElements .= "<meta name=\"description\" content=\"" . $this->header->description . "\" />" . PHP_EOL;

		#mobile scale meta if mobile is enabled
		if ($this->header->mobile)
			$headerElements .= "\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>" . PHP_EOL;

		#loading metas
		if ($this->header->metaList)
			foreach ($this->header->metaList as $property => $content)
				$headerElements .= "\t\t<meta property=\"".$property."\" content=\"".$content."\"/>" . PHP_EOL;

		#loading alternates
		if ($this->header->alternateList)
			foreach ($this->header->alternateList as $href => $lang)
				$headerElements .= "\t\t<link rel=\"alternate\" href=\"".$href."\" hreflang=\"".$lang."\"/>" . PHP_EOL;

		/*
			- should merge files and make a unique name file then check if the file already exists just
			include the file into content
		*/
		if (is_array($this->header->styleList))
			foreach ($this->header->styleList as $style)
				if ($style['load'])
				{
					$src = \Tool::cookFileUrl($style['file']);
					$headerElements .= "\t\t<link rel=\"stylesheet\" href=\"".$src;
					if ($style["version"]) $headerElements .= '?v='.$style["version"];
					$headerElements .= "\">" . PHP_EOL;
				}
		if (is_array($this->header->jsList))
			foreach ($this->header->jsList as $js)
				if ($js['load'] && $js['header']) {
					$src = \Tool::cookFileUrl($js['file']);
					$headerElements .= "\t\t<script src=\"".$src;
					if ($js['version']) 
						$headerElements .='?v='.$js['version'];
					$headerElements .= "\"></script>" . PHP_EOL;
				}
		return $headerElements;
	}

	private function build_body($template)
	{
		$template_rendered_content = $this->getRenderedTemplateFileContent($template);
		if (!$this->loadQueue) {
			$content = $template_rendered_content;
		} else {
			$content = false;
			foreach($this->loadQueue as $Qfile) {
				if (!$content) {
					$content = $this->getRenderedTemplateFileContent($Qfile);
				} else {
					if (strpos($content, QUEUE_LOAD_ELEMENT) !== false)
						$content = str_replace(QUEUE_LOAD_ELEMENT, $this->getRenderedTemplateFileContent($Qfile), $content);
					else 
						$content .= $this->getRenderedTemplateFileContent($Qfile);
				}
			}

			// fetch current template to the load queue
			if (strpos($content, QUEUE_LOAD_ELEMENT) !== false)
				$content = str_replace(QUEUE_LOAD_ELEMENT, $template_rendered_content, $content);
			else 
				$content .= $template_rendered_content;
		}

		/*
			- load js and scripts at the end of the page
		*/
		if (is_array($this->header->jsList))
			foreach ($this->header->jsList as $js)
				if ($js['load'] && !$js['header']) {
					$src = \Tool::cookFileUrl($js['file']);
					$content .= "\t\t<script src=\"".$src;
					if ($js['version']) 
						$content .='?v='.$js['version'];
					$content .= "\"></script>" . PHP_EOL;
				}
		$content .= $this->closeHTML();
		return $content;
	}

	private function closeHTML()
	{
		$string = PHP_EOL . "\t</body>" . PHP_EOL;
		$string .= "</html>" . PHP_EOL;
		return $string;
	}

	private function getRenderedTemplateFileContent($template)
	{
		$tempalteFileAddress = \Tool::cookFileAdress($template, "template") . TEMPLATE_EXTENSION;
		if (!is_file($tempalteFileAddress))
			return \Tool::error("there's no template file with the name " . $tempalteFileAddress);
		if (UI_FILE_CACHE) {
			$template_rendered_content = $this->fileCache->get($tempalteFileAddress);
			if ($template_rendered_content === false) {
				$template_rendered_content = $this->renderTemplateFile($tempalteFileAddress);
				$this->fileCache->set($tempalteFileAddress, $template_rendered_content);
			}
		} else {
			$template_rendered_content = $this->renderTemplateFile($tempalteFileAddress);
		}
		return $template_rendered_content;
	}

	private function renderTemplateFile($tempalteFileAddress)
	{
		$content = file_get_contents($tempalteFileAddress);
		$a = new \templating\PHPTranslate();
		return $a->render($content);
	}

	private function runMiddleware($content)
	{
		if (count($this->middleware) < 1)
			return $content;
		foreach($this->middleware as $middleware)
			$content = call_user_func($middleware, $content);
		return $content;
	}
}
