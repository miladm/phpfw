# introduction
Global store is a module that will manage global stores all over the project. for example you can create a global store for configurations
and it can easily load all over the project and seprate modules.

# installation
you have to ```include``` _./globalstore/\_setup.php_ where ever you know will load all over your project pages same as headers or footers.

# setup
to add new global store you have to build a class extends ```\Globalstore```

```php

class Config extends \Globalstore {}
```

# methods

## set
to add new key-value data.
### pramaters
* key => must be in string or integer format to name the variables
* value => it can be mixed (array, string, int and object)

```php
class Config extends \Globalstore {}

\Config::set( "name", "the name value" );
```

## get
to get the value back from global store

### parameters
* key : it's optional if it's ```null``` or ```false``` it will return the whole global store

```php
class Config extends \Globalstore {}

\Config::set( "name", "the name value" );

\Config::get( "name" ); // will return "the name value"
```