<?php
namespace tool\email;

define('ELASTICEMAIL_API_URL', \Config::get('elasticemail_api_url')?:'https://api.elasticemail.com/v2');
define('ELASTICEMAIL_API_KEY', \Config::get('elasticemail_api_key'));

class ElasticEmail
{
    private $apiURL = ELASTICEMAIL_API_URL;
    private $apiKey = ELASTICEMAIL_API_KEY;
    private $from = false;
    private $fromName = false;
    private $to = false;

    public function from(string $email, $name = false) :ElasticEmail
    {
        if (!\validator\Email::check($email))
            trigger_error('invalid email ' . $email);
        $this->from = $email;
        $this->fromName = $name;
        return $this;
    }

    public function to($email) :ElasticEmail
    {
        // if (!\validator\Email::check($email))
        //     trigger_error('invalid email ' . $email);
        $this->to = $email;
        return $this;
    }

    public function send($subject, $message, $htmlType = false)
    {
        if ((!$this->from && !\Config::get('email')) || !$this->to || !$subject || !$message ) {
            return false;
        }
        $post = [
            'from' => $this->from ?: \Config::get('email'),
            'fromName' => $this->fromName ?: (\Config::get('company')?:''),
            'apikey' => ELASTICEMAIL_API_KEY,
            'subject' => $subject,
            'to' => $this->to,
            'isTransactional' => false
        ];
        if ($htmlType)
            $post['bodyHtml'] = $message;
        else
            $post['bodyText'] = $message;


        $postdata = http_build_query($post);
        
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        
        $context  = stream_context_create($opts);
        
        return $result = file_get_contents($this->apiURL . '/email/send', false, $context);

        // try {
        //     $ch = curl_init();
        //     curl_setopt_array($ch, [
        //         CURLOPT_URL => $this->apiURL . '/email/send',
        //         CURLOPT_POST => true,
        //         CURLOPT_POSTFIELDS => $post,
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_HEADER => false,
        //         CURLOPT_SSL_VERIFYPEER => false
        //     ]);
            
        //     $result=curl_exec ($ch);
            
        //     die(var_dump(
        //         $result,

        //     ));
            
        //     curl_close ($ch);
        //     return $result;
        // } catch (Exception $ex) {
        //     \_E::set('elasticEmail', $ex->getMessage());
        //     return false;
        // }
    }
}
