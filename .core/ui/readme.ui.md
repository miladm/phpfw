| method    | parameters    | action    |
| ----      | ---           | ---       |
| meta | property, content | add meta tag to header |
| aleter | href, lang | add alternate meta tag to header for multi language site |
| mobile | set = true | add mobile scaling to header added by default |
| style | file, load = true, version = false |  add css style sheet to page |
| js | file, load = true, version = false, header = false | add js file to page. you can set to js be loaded at header using header parameter |
| setTitle | title | set page title |
| getTitle |  | get title |
| title | title | add string to the rest of title |
| description | description | add description meta to page |
| addQ | file, data | add a template page to queue |
| middleware | callable function | set a function as a middle ware before sending the data |

## middleware
middleware are to do things to content before printing for example do templating
__middle ware must return the content to next level__
```php
\UI::add("sample")->middleware(function($content) {
        return str_replace("something", "otherthing", $content);
    })->render("a@main");
```