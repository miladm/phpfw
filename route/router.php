<?php 
namespace route;

class Router
{
    // current request methods */
    private $method = false;
    private $requestData = false;
    private $request = null;
    private $requestHash = null;
    private $callback = null;
    private $value = false;

    // if there was a hash match (means exact match) it comes true
    private $hashMatch = false;

    // global methods */
    private $routeList = [];

    // define list */
    private $regex_shorthandName = [
        "number" => '[0-9]+',
        "numberstring" => '[0-9a-zA-Z]+',
        "string" => '[a-zA-Z]+',
        "url" => '[0-9a-zA-Z\_\-\%]+'
    ];

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'] ?? "GET";
        $this->requestData = new Request();
        $this->requestData->post = $this->method != "GET" ? $this->read_post() : false;
        $requestURI = \urldecode($_SERVER["REQUEST_URI"] ?? '');
        $url_path = parse_url($requestURI)["path"];
        $this->request = $this->current_request($url_path);
        $this->requestHash = $this->current_requestHash($url_path);
    }

    /**
     * setup middleware
     */
    public function use($routePattern, callable $middleware = null) :Router
    {
        if(\is_null($this->callback))
            $this->callback = new Response();
        if (is_null($middleware)) {
            if (is_callable($routePattern)) {
                $this->callback->use($routePattern);
            }
		} else {
            $routePattern = $this->replaceRegex($routePattern);
            if ($this->checkMiddlewareMatch($routePattern, $this->request)) {
                $this->callback->use($middleware);
            }
        }
        return $this;
    }

    /**
     * register a route to GET method
    */
    public function get($path, callable $callback, $value = false)
    {
        $path = $this->replaceRegex($path);
        return $this->registerRoute("GET", $path, $callback, $value);
    }

    /**
     * register a route to post method
     */
    public function post($path, callable $callback, $value = false)
    {   
        $path = $this->replaceRegex($path);
        return $this->registerRoute("POST", $path, $callback, $value);
    }

    /**
     * register a route to post method
     */
    public function add($path, callable $callback, $value = false)
    {   
        $this->get($path, $callback, $value);
        return $this->post($path, $callback, $value);
    }

    /**
     * set middle ware for
     */
    /**
     * call the callback for current request
     */
    public function start($autoLoaderFunction = false)
    {
        if ($this->method == 'OPTIONS') 
            die("Welcome Darling. We detected you :)");
        if (!$this->callback || !$this->callback->callable()) {
            if(\is_null($this->callback))
                $this->callback = new Response();
            if (
                !$autoLoaderFunction 
                || !\call_user_func(
                    $autoLoaderFunction,
                    $this->request,
                    $this->requestData,
                    $this->callback,
                    $this->method
                )
            ) {
                (new \route\Response(function(){}))->error404();
            }
        } else {
            $this->callback->run( $this->requestData );
        }
    }

    /**
     * check if dir has not set before in get requests only
     */
    public function isFree($path)
    {
        $path = $this->replaceRegex($path);
        return !isset($this->routeList["GET"][ md5($path) ]);
    }

    public function getRouteList()
    {
        return $this->routeList;
    }

    /**
     * private classes
    */
    /**
     * detect the post method and return the value
     */
    private function read_post()
    {
        if (count($_POST))
            return (object)$_POST;
        $input = file_get_contents("php://input");
        if ($input != null) {
            if (function_exists('getallheaders')) {
                $headers = getallheaders();
            } else {
                $headers = $_SERVER;
            }
            $contentType = $headers['CONTENT_TYPE'] ?? $headers["Content-Type"] ?? false;
            if (strpos(strtolower($contentType), 'application/json') !== false)
                return json_decode($input);
            else
                return  $input;
        }
        return false;
    }

    /**
     * current request path
     */
    private function current_request($url_path)
    {
        $con = $this->skipRootDir($url_path);
        if(strlen($con) == 0 || $con[\strlen($con) - 1] != "/")
            $con .= "/";
        return $con;
    }

    /**
     * current request path in hash
     */
    private function current_requestHash($url_path)
    {
        $con = $this->skipRootDir($url_path)."/";

        return md5($this->tool_prevent_slash($con));
    }

    /**
     * this will skip the root dir in case the fw is in an special directory
     */
    private function skipRootDir($value)
	{

		// getting the root of project
		// $value = strtolower($value);
		// $mainRootUri = strtolower($_SERVER["SCRIPT_NAME"]);
        // $endPositionOfMainDirectory =  strlen($mainRootUri) -  strlen("index.php");
        // $value = substr($value, $endPositionOfMainDirectory - 1);
        return $value == "/" ? "" : $value;
    }

    /**
     * compile the string mode path to regex
     */
    private function replaceRegex($path)
    {
        // $path = strtolower($path);

        // make sure the path starts and stop with slash "/"
        if ($path == "" || $path[0] != "/") 
            $path = "/". $path;
        if ($path[ strlen($path) - 1 ] != "/") 
            $path .= "/";

        // replace all reserved regex like /:name(string)/
        foreach($this->regex_shorthandName as $shorthand => $regex)
            $path = preg_replace(
                "/\/:(". $this->regex_shorthandName["url"] .")\((". $shorthand .")\)\//",
                "/(?<$1>". $regex .")/",
                $path
                );
        
        $path = preg_replace('/\/:(?<name>[^(]*)\((?<pattern>[^)]*)\)/', "/(?<$1>$2)", $path);
        
        // replace all unknown regex parameters like /:name/
        $path = preg_replace(
            "/:(". $this->regex_shorthandName["url"] .")\//",
            "(?<$1>". $this->regex_shorthandName["url"] .")/",
            $path
            );

        // replace all slashes
        $path = $this->tool_prevent_slash($path);
        return $path;
    }

    private function tool_prevent_slash($string)
    {
        return str_replace("/", "\/", $string);
    }

    /**
     * do the registering route process
     */
    private function registerRoute($method, $path, $callback, $value): Response
    {
        if(!is_null($this->callback))
            $callback_object = clone($this->callback);
        else
            $callback_object = new Response();

        //  if there's a hash match then there's no reason to match more
        if (!$this->hashMatch)
            if ($this->compareWithCurrentRequest( $method, $path)) {
                $this->callback = $callback_object->setCallback($callback, $value);
            }
        $this->routeList[ $method ][ $path ] = &$callback_object;
        $this->routeList[ $method ][ md5($path) ] = &$callback_object;
        return $callback_object;
    }

    /**
     * check if the current request match the added pattern and if so reserve the callback in call$callback
     */
    private function compareWithCurrentRequest($method, $path)
    {
        if ($method != $this->method)
            return false;

        /* check for a hash match as it's a priority */
        if ($this->hashMatch($path))
            return true;
        if (!$this->checkRoute($path, $this->request))
            return false;
        return true;
    }

    /**
     *  check the given middleware match current request
    */
    private function checkMiddlewareMatch($path, $routeString)
    {
         if (preg_match("/^". $path ."/", $routeString))
            return true;
        return false;
    }
    /**
     *  check the given route pattern with the string of request
    */
    private function checkRoute($path, $routeString)
    {
         if (preg_match("/^". $path ."$/", $routeString, $matchList)) {
            $this->requestData->param = (object) $matchList;
            return true;
         }
        return false;
    }

    /**
     * check if the hash of route and request match it will true the hash match flag
     */
    private function hashMatch($path)
    {
        if (md5($path) == $this->requestHash) {
            $this->hashMatch = true;
            return true;
        }
        return false;
    }

}