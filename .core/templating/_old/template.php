<?php

class template extends brain
{
	private $file;
	private $languageName = false;
	public $content;

	private $masters;

	function __construct( $file = null )
	{
		if($file)
			$this->file = $file;
	}

	public function returnMake(  )
	{
		return $this->read()->cook()->content;
	}

	public function language( $languageName = false )
	{
		$this->languageName = $languageName;
		return $this;
	}

	public function make( $file = null ){
		if(!$this->file && $file)
			$this->file = $file;
		$this->putContent( );
	}

	public function putContent(  )
	{
		$langMark = $this->languageName ? ".".$this->languageName : "" ;

		if(strpos($this->file, "@"))
		{
			$exfile = explode("@", $this->file);
			$file = $this->conf['outDir'].$exfile[1]."/".$exfile[0].$langMark.$this->conf['outFor'];
		}
		else
			$file = $this->conf['outDir'].$this->file.$langMark.$this->conf['outFor'];


		$z = explode("/", $file);
		$newDir = "";
		foreach ($z as $key => $value)
		{
			if( $key == count($z) - 1) break;
			if( !is_dir( $newDir.$value ) )
				mkdir($newDir.$value, 0755);
			$newDir .= $value."/";
		}

		$cookingData = $this->read();
		if( $this->languageName ) $cookingData->translate( $this->languageName );

		file_put_contents($file, $cookingData->cook()->content);
	}

	private function read( )
	{
		if(strpos($this->file, "@")){
			$exfile = explode("@", $this->file);
			$tmp = "build/".$exfile[1]."/".$this->conf['tmpDir'].$exfile[0].$this->conf['tmpFor'];
		}
		else
			$tmp = "build/main/".$this->conf['tmpDir'].$this->file.$this->conf['tmpFor'];
		if (file_exists( $tmp ))
		{
			$this->content = file_get_contents($tmp);
			$this->join()->fetch();
			return $this;
		}else SCRIPTERROR("'".$tmp."' Not found!");
		$this->e[] = "er.template.tmpNotExists";
		return false;
	}

	private function join(  )
	{
		if($matches = $this->matches("/\<load ([^>]*)\>/", $this->content))
			foreach ($matches as $loader)
			{
				$file = new template( $loader );
				$this->content = str_replace("<load ".$loader.">", $file->read()->content, $this->content);
			}
		return $this;
	}

	private function fetch(  )
	{
		#check master's master if exists
		if(!$this->masters['name'] = $this->matches("/\<master:([^>]*)\>/", $this->content)) return $this;

		foreach ($this->masters['name'] as $key => $master) {
			$file = new template( $master );
			$masterCont = $file->read()->fetch()->content;

			$master =str_replace("/", "\/", $master);
			#read variables
			$variables = $this->matches("/\<dyn:([^>\/]*)[\/]*\>/", $masterCont);
			#find values
			if($variables) foreach ($variables as $var) {
				$value = $this->matches("/<master:$master>.*?<$var>(.*?)<\/$var>.*?<\/master:$master>/s", $this->content);
				$data[$var] = $value[0];
			}


			#set values beside variables in master
			foreach ($data as $var => $val) {
				$masterCont = preg_replace("/\<dyn:".$var."[\/]*\>/", $val, $masterCont);
			}
			#include master
			$this->content = preg_replace("/\<master:$master\>.*?<\/master:$master\>/s", $masterCont, $this->content, 1);
		}
		#read and find all parameters needed in the master
		return $this;
	}

	private function cook( )
	{

		$ifMethods = [
		/*call fucntion*/
			"/\<\:([^>(]*)\(\)\>/" => "<?php $1() ?>",
			"/\<\:([^>(]*)\(([^).$'\"]*)\)\>/" => "<?php $1('$2') ?>",
			"/\<\:([^>(]*)\(([^.$)]*)\.([^)]*)\)\>/" => "<?php $1($$2->$3) ?>",
			"/<:([^>(]*)\(([^.)]*)\.([^)]*)\)>/" => "<?php $1($2->$3) ?>",
			"/\<\:([^>]*)[ ]*\(([^)>]*)\)\>/" => "<?php $1($2) ?>",
			"/<\?php (\w*\d*)\.(\w*\d*)\(/" => "<?php $$1->$2(",
			// "//"
 				// furl function shortcodes
			"/\<furl\(([^)]*)\)\>/" => "<?php furl('$1') ?>",
			"/\<furl ([^>]*)\>/" => "<?php furl('$1') ?>",
			"/\<#\(([^)]*)\)\>/" => "<?php furl('$1') ?>",
		/*echo variable in place method*/
			"/\<\:([^.@>(]*)\.([^>]*)\.([^>]*)\.([^>]*)\.([^>]*)\>/" => "<?php echo @$$1->$2->$3->$4->$5 ?>", 
			"/\<\:([^.@>(]*)\.([^>]*)\.([^>]*)\.([^>]*)\>/" => "<?php echo @$$1->$2->$3->$4 ?>",
			"/\<\:([^.@>(]*)\.([^>]*)\.([^>]*)\>/" => "<?php echo @$$1->$2->$3 ?>",
			"/\<\:([^.@>(]*)\.([^>]*)\>/" => "<?php echo @$$1->$2 ?>",
			"/\<\:([^.@>(]*)@([^.>]*)\.([^>]*)\>/" => "<?php echo @$$2->$3['$1'] ?>",
			"/\<\:([^@>(]*)@([^>]*)\>/" => "<?php echo @$$2['$1'] ?>",
			"/\<\:([^.@>(]*)\>/" => "<?php echo @$$1 ?>",
			/*
				- have to add array[$var]
			*/
		/*if methods fixture*/
			"/\<!([a-zA-Z0-9]+)[ ]*\>/" => "<?php if(!isset($$1)||!@$$1): ?>", //normal variable
			"/\<if[ ]+([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php if(@$$1): ?>", //normal variable
			"/\<if[ ]+([a-zA-Z0-9_\-]*)\(([a-zA-Z0-9_\-]*)\)[ ]*\>/" => "<?php if(@$1($$2)): ?>", //normal variable
			"/\<if[ ]+([a-zA-Z0-9_\-]*)@([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php if(@$$2['$1']): ?>", //array variable
			"/\<if[ ]+([a-zA-Z0-9_\-]*).([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php if(@$$1->$2): ?>", //normal variable
			"/\<if[ ]+([a-zA-Z0-9_\-]*)@([a-zA-Z0-9_\-]*).([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php if(@$$2->$3['$1']): ?>", //normal variable
			"/\<if[ ]+[$]([a-zA-Z0-9_\-]*)@([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php if(@$$2[$$1]): ?>", //array variable
			/**
				- ?
			**/
			"/\<if[ ]+([a-zA-Z0-9_\-:]*)(\([^\)]*\)|)[ ]*\>/" => "<?php if(@$1$2): ?>",
			"/\<else[ ]+([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php elseif(@$$1): ?>",
			"/\<else[ ]+([a-zA-Z0-9_\-]*)@([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php elseif(@$$2['$1']): ?>",
			"/\<else[ ]+[$]([a-zA-Z0-9_\-]*)@([a-zA-Z0-9_\-]*)[ ]*\>/" => "<?php elseif(@$$2[$$1]): ?>",
			/*
				- ?
			*/
			"/\<else[ ]+([a-zA-Z0-9_\-:]*)(\([^\)]*\)|)( )*\>/" => "<?php elseif(@$1$2): ?>",
			"/\<else( )*>/" => "<?php else: ?>",
			"/\<\/if( )*>/" => "<?php endif ?>",
		/*foreach methods fixture*/
			"/\<foreach[ ]+([a-zA-Z0-9_\-]+)([ ]+| in )([a-zA-Z0-9_\-]+)\>/" => "<?php foreach($$3 as \$key => $$1): ?>",
			"/\<foreach[ ]+([a-zA-Z0-9_\-]+)([ ]+| in )([a-zA-Z0-9_\-]+)@([a-zA-Z0-9_\-]+)\>/" => "<?php foreach($$4['$3'] as \$key => $$1): ?>",
			"/\<foreach[ ]+([a-zA-Z0-9_\-]+)([ ]+| in )([a-zA-Z0-9_\-]+).([a-zA-Z0-9_\-]+)\>/" => "<?php foreach($$3->$4 as \$key => $$1): ?>",
			"/\<foreach[ ]+([a-zA-Z0-9_\-]+)([ ]+| in )([a-zA-Z0-9_\-]+)@([a-zA-Z0-9_\-]+).([a-zA-Z0-9_\-]+)\>/" => "<?php foreach($$4->$5['$3'] as \$key => $$1): ?>",
			"/\<foreach[ ]+([a-zA-Z0-9_\-]*)\>/" => "<?php foreach($$1 as \$key => \$val): extract(\$val); ?>",
			"/\<\/foreach( )*>/" => "<?php endforeach ?>",
		/*direction fixture*/
			"/\_url\_(\/|)/" => $this->conf['dir'],
			"/\_dir\_(\/|)/" => $this->conf['dir']."f/".$this->conf['tmpDir'],
		/*php include*/
			"/\<include ([^>]*)[ ]*\>/" => "<?php include '$1.php' ?>",
		/* esciping untranslated*/
			"/\{\{([^}]*)\}\}/" => "$1",
			];
		foreach ($ifMethods as $key => $value)
			$this->content = preg_replace($key, $value, $this->content);
		/*load tmp files inside itself*/
		return $this;
	}

	private function translate ( $languageName )
	{
		$targetLanguageFile = $this->conf["languageDir"].$languageName.".json";
		if(!is_file( $targetLanguageFile )) SCRIPTERROR( "language file : " . $targetLanguageFile . " not Exists~!" );

		$translateLibrary = (array) json_decode( file_get_contents( $targetLanguageFile ) );

		foreach( $translateLibrary as $original => $translation )
		{
			$this->content = str_replace( "{{".$original."}}", $translation, $this->content );
		}

		return $this;
	}

	private function matches( $r_e, $content )
	{
		if(preg_match_all($r_e, $content, $m)) return $m[1];
		return false;
	}
}
