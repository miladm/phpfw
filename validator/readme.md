# validator
validator is in case you wanna check some inputs

# available validations
* integer
* boolean
* email
* url

-- non available
* date

# how to use
easily call the static _check_ method of correct validator.

here's example for email validation
```php
$email = $_POST['email'];
if (!\validator\Email::check($email)) {
    // trigger error or something else
} else {

}
```

# add new validator
you must create a file with the name of validator and with the namespace of validator into _validator_ directory.
then you have to put a class with the name of your target validator, extends the main `\validator\Validator` and define required parameters into the class,

step 1 create the class into /core/validator/<validator_name_in_lowwercase>.php
namespace it as `validator` and define class:
```php
namespace validator;

class Int extends \validator\Validator
{
    ....
}
```
step 2 choose the method you wanna use.

there are the methods below :
* filter
    to use the filters available for php (like Email filter. check `validator/email.php`)
* regex
    the regular expresion to validator
* function
    you can set a method (function) with the name `validator` and return `true` or `false` if it matches.
    function parameter is a single `$value`

## filter
```php
namespace validator;

class Email extends \validator\Validator
{
    protected $pattern = FILTER_VALIDATE_EMAIL;
    protected $type = 'filter';
}
```

## regex
```php
<?php
namespace validator;

class Email extends \validator\Validator
{
    protected $pattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
    protected $type = 'regex';
}
```

## function
```php
<?php
namespace validator;

class Boolean extends \validator\Validator
{
    protected $type = 'function';
    protected function validator($value)
    {
        return is_bool($value);
    }
}
```