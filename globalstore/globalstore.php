<?php
namespace globalstore;

class Globalstore
{
	public static function set($key, $value)
	{
		self::getDatastore()->set($key, $value);
	}

	public static function setAll(array $data)
	{
		foreach ($data as $key => $value)
			self::getDatastore()->set($key, $value);
	}

	public static function get($key = false)
	{
		return self::getDatastore()->get($key);
    }
	
	public static function save($filePath)
	{
		return self::getDatastore()->save($filePath);
	}

    private static function getDatastore()
    {
        global $globalDataStore;
		$className = get_called_class();
        if (!isset($globalDataStore[$className]))
            $globalDataStore[$className] = new \globalstore\Collection;
        return $globalDataStore[$className];
    }
}
$globalDataStore = [];
