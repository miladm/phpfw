<?php

// configs global store */
class Config extends \globalstore\Globalstore
{
    static function loadJsonFile($fileAddress)
    {
        $data = json_decode(file_get_contents($fileAddress), true);
        \Config::setAll($data);
    }
}
if (file_exists(CONFIGFILE_DIR)) {
    Config::loadJsonFile(CONFIGFILE_DIR);
} else {
    include ".default/configs.php";
    Config::save(CONFIGFILE_DIR);
}


// to fix bad configurations */
\Tool::config_polish();
