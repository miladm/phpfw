<?php
namespace route;

class Request
{
    public $get      = null;
    public $post     = null;
    public $file     = null;
    public $param    = null;
    public $session  = null;
    public $cookie   = null;
    public $attachment = [];

    function __construct()
    {
        $this->get  = count( $_GET ) ? (object)$_GET : null;
        $this->file = count( $_FILES ) ? (object)$_FILES : null;
        if (!\tool::checkCli()) {
            $this->session = count( $_SESSION ) ? (object)$_SESSION : null;
            $this->cookie = count( $_COOKIE ) ? (object)$_COOKIE : null;
        }
    }

    public function attach(array $data): Request
    {
        $this->attachment += $data;
        return $this;
    }

}