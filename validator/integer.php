<?php
namespace validator;

class Integer extends \validator\Validator
{
    protected $type = 'function';
    protected function validator($value)
    {
        return is_int($value);
    }
}
