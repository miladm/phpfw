<?php
namespace globalstore;

class Collection
{
    private $data = false;

    public function set($key, $value)
    {
        $this->data[ $key ] = $value;
    }

    public function get($key = false)
    {
        if (!$key) 
            return $this->data;
        return $this->data[ $key ] ?? false;
    }

    public function save($filePath)
    {
        $return = file_put_contents($filePath, json_encode($this->data, JSON_PRETTY_PRINT));
        chmod($filePath, 0777);
        return $return;
    }
}

