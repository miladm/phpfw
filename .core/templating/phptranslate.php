<?php
namespace templating;

class PHPTranslate extends Templating
{
    public function translateList()
    {
        return [
            '/\$\{\{([^}#\/]+)\}\}/' => [
                '/^([a-zA-Z].*)$/' => '<?php echo $$1 ?? ""; ?>',
                '/\.([a-zA-Z][^.]*)/' => '->$1',
                '/\[([a-zA-Z][\w]*)/' => '[$$1'
            ],

            '/\$\{\{\#([^}]+)\}\}/' => [
                '/^else$/' => '<?php } else { ?>',
                '/^([a-zA-Z][^ ]*)$/' => '<?php if ($$1 ?? false) { ?>',
                '/^([a-zA-Z].*)[ ]+as[ ]+([a-zA-Z][\w]*)$/' => '<?php foreach ($$1 ?? [] as $index => $$2) { ?>',
                '/\.([a-zA-Z][^.]*)/' => '->$1',
                '/\[([a-zA-Z][\w]*)/' => '[$$1'
            ],
            '/\$\{\{\/([^}]+)\}\}/' => [
                '/^(.*)$/' => '<?php } ?>',
            ]
        ];
    }
}