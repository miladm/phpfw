<?php
namespace prototype;

/*
    prototype is the pre-built data-access layer
 */
//because it's the data-access layer and it will implement table in itself
class IPrototype
{
    private $_schema = false;
    public $table = null;
    public $resultMapping = [];
    private $mapGroupFields = null;

    /*
        prototype methods are ....
     */
    public function create()
    {
        // die(var_dump(
        //     $this->schema()->init_query()
        // ));
        return $this->table->query($this->schema()->init_query());
    }

    /**
     *  @return IPrototype
     *  @return boolean 
     */
    // public function where($condition)
    // {
    //     if (is_null($this->table))
    //         die ('that was not suppose to happen .. visit iPrototype.php @where method');
    //     $this->table = $this->table->where($condition[0], $condition[1]);
    //     return $this;
    // }

    /**
     * @return IPrototype
     * 
     */
    public function groupBy($column)
    {   
        if (is_null($this->table))
            die ('that was not suppose to happen .. visit iPrototype.php @groupBy method');
        $this->table = $this->table->group($column);
        return $this;
    }

    public function setAlignFetch($schemaName)
    {
        $this->alignFetch = $schemaName;
    }

    public function add(array $data)
    {
        $validatedData = $this->validate($data);
        if (!$validatedData)
            return false;
        if (count($validatedData) <= 0)
            return false;
        return $this->table->insert($validatedData);
    }

    public function validate(array $data, bool $checkNotNull = true)
    {
        return $this->schema()->fetchData($data, $checkNotNull);
    }

    public function update(array $newData, array $condition) : int
    {
        if (count($newData) <= 0)
            return false;
        $validatedData = $this->validate($newData, false);
        if (count($condition) == 2)
            $t = $this->table->where($condition[0], $condition[1]);
        else
            $t = $this->table;
        $schema = $this->schema();
        foreach ($schema->leftjoinList() as $value)
            $t = $t->leftjoin($value->name, $value->on, $value->asName);
        return $t->update($validatedData);
    }

    public function map(array $map) : \prototype\IPrototype
    {
        $progressMapping = [];
        foreach ($map as $title => $fieldName) {
            if (is_array($fieldName)) {
                foreach ($fieldName as $fieldSubTitle => $fieldSubName) {
                    $subFieldMapping[$fieldSubTitle] = $this->mapFieldParser($fieldSubName);
                }
                $progressMapping[$title] = $subFieldMapping;
            } else {
                $progressMapping[$title] = $this->mapFieldParser($fieldName);
            }
        }
        $this->resultMapping = $progressMapping;
        return $this;
    }

    private function mapFieldParser($fieldName)
    {
        if (strpos($fieldName, '.') == false) {
            return $fieldName;
        } else {
            list($tableName, $tableField) = explode('.', $fieldName);
            if ($tableName == $this->schemaName()) {
                return $tableField;
            } else {
                return "schema_" . $tableName . "_" . $tableField;
            }
        }
    }

    public function mapGroup($fieldName) : \prototype\IPrototype
    {
        if (!$this->resultMapping) {
            \Tool::error('there must be a mapping for mapGroup');
            return $this;
        }
        $this->mapGroupFields = $this->mapFieldParser($fieldName);
        return $this;
    }

    public function get(array $condition = [], $limit = false, $start = false, $asc = true)
    {
        $t = $this->table->fetchArray();

        /*
            do the lef joinings
         */
        $schema = $this->schema();
        foreach ($schema->leftjoinList() as $value) {
            $t = $t->leftjoin($value->name, $value->on, $value->asName);
        }
        if (count($condition) == 2) {
            $t = $t->where($condition[0], $condition[1]);
        }
        $asc = ($asc === false || $asc === "dasc") ? false : true;
        if (is_int($limit) && $limit > 0)
            if (is_int($start))
            $t = $t->limit($start . "," . $limit);
        else
            $t = $t->limit($limit);
        $resultList = $t->order($schema->getName() . "." . $schema->getKey(), $asc)->select($schema->selectList());
        $resultList = $this->schemaResult($resultList);
        if ($resultList) {
            return $resultList;
        } else {
            return false;
        }
    }

    public function getById(int $id)
    {
        $idFieldName = $this->schema()->getName() . '.id';
        return $this->getOne([$idFieldName . '=?', $id]);
    }

    public function getOne(array $condition = [], $asc = true)
    {
        $result = $this->get($condition, 1, 0, $asc);
        if (is_array($result))
            return $result[0];
        return $result;
    }

    public function getFirst(array $condition = [])
    {
        return $this->getOne($condition);
    }

    public function getLast(array $condition = [])
    {
        return $this->getOne($condition, false);
    }

    public function count(array $condition = [])
    {
        if (count($condition) == 2)
            $t = $this->table->where($condition[0], $condition[1]);
        else
            $t = $this->table;
        $schema = $this->schema();
        foreach ($schema->leftjoinList() as $value)
            $t = $t->leftjoin($value->name, $value->on, $value->asName);
        return $t->count();
    }

    public function sum(array $condition = [], $column)
    {
        $t = $this->table;
        if (count($condition) > 0) {
            $t = $t->where($condition[0], $condition[1]);
        }
        $schema = $this->schema();
        foreach ($schema->leftjoinList() as $value)
            $t = $t->leftjoin($value->name, $value->on, $value->asName);
        $sum = $t->select('sum(traffic) as sum')[0]->sum;
        return is_null($sum) ? 0 : $sum;
    }

    public function expect(array $condition = [], $number = 1)
    {
        return $number == $this->count($condition);
    }

    public function delete(array $condition = [])
    {
        $t = $this->table;
        if (count($condition) > 0) {
            $t = $t->where($condition[0], $condition[1]);
        } else {
            return false;
        }
        $schema = $this->schema();
        foreach ($schema->leftjoinList() as $value)
            $t = $t->leftjoin($value->name, $value->on, $value->asName);
        return $t->delete();
    }

    /**
     * @return IPrototype
     * @return boolean
     */
    private function createRelationIPrototype($relationStructure, $id)
    {
        $name = $relationStructure['relationSchemaName'] ? : $relationStructure['schemaName'];
        $namespace = $relationStructure['namespace'] ?? null;

        if (!is_null($namespace)) {
            if (class_exists($namespace . $name)) {
                $targetClassName = $namespace . $name;
            }
        } elseif (class_exists($name)) {
            $targetClassName = $name;
        } elseif (class_exists('\\' . strtolower($name) . '\\' . $name)) {
            $targetClassName = '\\' . strtolower($name) . '\\' . $name;
        } else {
            return false;
        }
        $object = (new $targetClassName())::IPrototype();
        $nameOnSchema = $relationStructure['nameOnSchema'] ? : $this->schema()->getName();
        $objectSchemaName = $object->schema()->getName();
        $condition = ["`$objectSchemaName`.`$nameOnSchema`=?", $id];
        $object->where($condition);
        return $object;
    }

    private function schemaResult($result)
    {
        if (!$result)
            return false;
        if ($this->resultMapping) {
            $fetchSchema = $this->resultMapping;
        } else {
            $fetchSchema = $this->schema()->fetchSchema();
        }
        $jsonList = $this->schema()->getJsonList();
        $finalData = [];
        foreach ($result as $row) {
            
            // handling groupBy if exists
            if ($this->mapGroupFields) {
                $keyName = $row[$this->mapGroupFields];
                if (!isset($groupedFinalData[$keyName])) {
                    $groupedFinalData[$keyName] = $this->fetch($fetchSchema, $row, $jsonList);
                } else {
                    foreach ($fetchSchema as $fieldName => $fieldSchema) {
                        if (is_array($fieldSchema)) {
                            $groupedFinalData[$keyName]->{$fieldName}[] = $this->fetch($fieldSchema, $row, $jsonList);
                        }
                    }
                }
            } else {
                $finalData[] = $this->fetch($fetchSchema, $row, $jsonList);
            }
        }
        if (!$finalData && isset($groupedFinalData)) {
            foreach ($groupedFinalData as $row) {
                $finalData[] = $row;
            }
        }
        return $finalData;
    }

    private function fetch($fetchSchema, $row, $jsonList = [])
    {
        $data = [];
        foreach ($fetchSchema as $name => $keyName) {
            if (is_array($keyName)) {
                $data[$name] = [$this->fetch($keyName, $row, $jsonList)];
            } elseif (is_object($keyName)) {
                if (isset($keyName->id) && is_null($row[$keyName->id])) {
                    $data[$name] = null;
                    continue;
                } else {
                    $data[$name] = $this->fetch($keyName, $row, $jsonList);
                }
            } else {
                $data[$name] = $row[$keyName];
            }
            if (count($jsonList) > 0)  {
                if (in_array($keyName, $jsonList)) {
                    $data[$name] = json_decode($data[$name]);
                }
            }
        }
        return (object)$data;
    }

    /**
     *  @return string
     */
    public function schemaName($name = false)
    {
        return $this->_schema->getName();
    }

    /**
     *  @return \prototype\Schema
     */
    public function schema($name = false)
    {
        if (!$name && !$this->_schema)
            return \Tool::error("you must set the schema name first time you are setting it.");
        if (!$this->_schema)
            $this->_schema = new \prototype\Schema($name);
        return $this->_schema;
    }

    public function setupTable()
    {
        $this->table = new \prototype\SchemaTable($this->schema());
        return true;
    }
}
