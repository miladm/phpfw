# methods
| method | explanation |
| ---   | ---- |
| add($languageName, $languageJSONFILE) | to add new language |
| load($languageName) | to load language file(library) |

## setup languages
```php
\Language::add("fa", "fa.json@lang")
    ->add("en", "en.json@lang")
    ->add("en", "en.json@lang")
    ->add("ar", "ar.json@lang")
    ->add("ar", "ar.json@lang")
    ->add("es", "es.json@lang");
```
## use language translator
```php
\Language::load("fa")->render("template@main");
```

# language file (library)
language file is a json file for trans lation