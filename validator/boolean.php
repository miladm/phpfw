<?php
namespace validator;

class Boolean extends \validator\Validator
{
    protected $type = 'function';
    protected function validator($value)
    {
        return is_bool($value) || $value == 1 || $value == 0;
    }
}
