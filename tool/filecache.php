<?php
namespace tool;

define("FILE_CACHE_ROOT_DIR", "../.file");

class FileCache
{
    private $registerName = null;

    function __construct( string $name )
    {
        $this->registerName = $name;
    }

    public function set($fileName, $data)
    {
        $fileName = \str_replace('/', "_", $fileName);
        $fileName = \str_replace('\\', "_", $fileName);
        $this->registerName = \str_replace('/', "_", $this->registerName);
        $this->registerName = \str_replace('\\', "_", $this->registerName);
        $file = FILE_CACHE_ROOT_DIR . "/" . $this->registerName . "/" . $fileName;
        \Tool::force_file_put_contents($file, json_encode($data));
    }
       
    public function get($fileName)
    {
        $fileName = \str_replace('/', "_", $fileName);
        $fileName = \str_replace('\\', "_", $fileName);
        $this->registerName = \str_replace('/', "_", $this->registerName);
        $this->registerName = \str_replace('\\', "_", $this->registerName);
        $file = FILE_CACHE_ROOT_DIR . DIRECTORY_SEPARATOR . $this->registerName . DIRECTORY_SEPARATOR . $fileName;
        if (is_file($file))
            return json_decode(\file_get_contents($file));
        else 
            return false;
    }
}