<?php

namespace route;

class InputExpectation
{
    public static function check(\route\Request $request, \route\Response $response, $schema)
    {
        foreach ($schema as $key => $scheme) {
            if (!isset($request->{$key})) {
                self::Error($schema);                
                $response->error400();
            } else {
                foreach ($scheme as $name => $type) {
                    if (!isset($request->{$key}->{$name})) {
                        self::Error($schema);
                        $response->error400();
                    } else {
                        continue;
                    }
                }
            }
        }
        $response->next();
    }

    private static function Error($schema)
    {
        fwrite(STDERR, "error: InputExpectation on input check");
        fwrite(STDERR, "expected Shema: " . json_encode($schema, JSON_PRETTY_PRINT) . PHP_EOL);
    }
}
