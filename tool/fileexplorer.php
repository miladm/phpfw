<?php
namespace tool;

/**
 * handles files
 *  -> header file
 *  -> push file force download 
 */

define( "READ_CHUNK", 1024 * 1024 );

class FileExplorer
{
    private $efficient = false;
    private $file = null;
    private $name = null;
    private $extension = null;
    private $size = 0;
    private $mimeType = null;
    private $cacheAble = true;
    private $request_range = [];
    private $length = 0;

    function __construct( string $file )
    {
        $this->efficient = file_exists( $file );

        if( !$this->efficient ) return false;
        
        $this->file = $file;
        $this->size = filesize( $this->file );
        $this->mimeType = mime_content_type( $this->file );
        
        // exclude name and extension
        \preg_match( "/([^\/]+)\.([\w\d]+)$/", $this->file, $matchList );
        $this->name = $matchList[1];
        $this->extension = $matchList[2];

    }

    public function print()
    {
        if( !$this->efficient ) return false;
        
        if (!($FILE = fopen($this->file, 'rb'))) 
            return false;
        
        \ob_clean();
        $bytesToRead = READ_CHUNK; //1MB per round

        $this->create_header();

        $index = $this->request_range["start"];

        fseek($FILE, $index);

        set_time_limit(0);

        while(!feof($FILE) && $index <= $this->request_range["end"])
        {
            if(($index + $bytesToRead) > $this->request_range["end"])
                $bytesToRead = $this->request_range["end"] - $index + 1;
            
            $data = fread($FILE, $bytesToRead);
            echo $data;
            ob_flush();
            flush();
            $index += $bytesToRead;
        }

        fclose($FILE);
        die();
    }

    private function create_header()
    {
        /**
         * sending file header
         */
        
        // content type and content description header
        header('Content-Type: '. $this->mimeType );

        if( $this->cacheAble )
            $this->browserCache_header();

        //last modified
        header("Last-Modified: ".gmdate('D, d M Y H:i:s', @filemtime( $this->file )) . ' GMT' );

        //if it's font or a descrion needed extension then should header content description here
        $this->description_header();

        // check if there's a request range and if not set the range headers 
        $this->requesetRange_header();

        //the length for the output file.
        header('Content-Length: '.$this->length);

    }

    private function browserCache_header()
    {
        header('Pragma: public');
        header('Cache-Control: max-age=1000000');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 1000000));
    }

    private function description_header()
    {
        if( in_array($this->extension, ["eot", "ttf", "woff", "otf"]) )
        {
            switch( $this->extension ){
                case "eot":
                    header('Content-Description: Font File');
                    header('Content-Description: Extended OpenType Font File');
                    break;
                case "ttf":
                    header('Content-Description: Font File');
                    header('Content-Description: TTF Font File');
                    break;
                case "woff":
                    header('Content-Description: Font File');
                    header('Content-Description: Web Font File');
                    break;
                case "woff2":
                    header('Content-Description: Font File');
                    header('Content-Description: Web Font File');
                    break;
                case "otf":
                    header('Content-Description: Font File');
                    header('Content-Description: OpenType Font File');
                    break;              
            }
        }

    }

    private function requesetRange_header()
    {   
        $this->request_range = [
            "start" => 0,
            "end" => $this->size - 1
        ];
        $this->length = $this->size;
        

        header("Accept-Ranges: bytes");
        // header("Accept-Ranges: 0-". $this->request_range["end"]);

        //check if there's any request range
        if (isset($_SERVER['HTTP_RANGE'])) 
        {
            list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);

            if ( strpos($range, ',') !== false ) 
            {
                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header("Content-Range: bytes ". $this->request_range["start"] ."-". $this->request_range["end"] ."/". $this->size );

                exit;
            }

            $start = $this->request_range["start"];
            $end = $this->request_range["end"];

            if ($range[0] == '-')
                $start = $this->size - (int) substr($range, 1);

            elseif( $range[ \strlen( $range ) - 1 ] == "-" )
                $start = (int) substr( $range, 0, strpos( $range, "-") );
                
            else
            {
                $range = explode('-', $range);
                $start = $range[0];
                $end = ( isset($range[1]) && is_numeric($range[1]) ) ? $range[1] : $this->request_range["end"];
            }
            
            $end = $end > $this->request_range["end"] ? $this->request_range["end"] : $end;

            if ( $start > $end || $start > $this->size - 1 )
            {
                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header("Content-Range: bytes ". $this->request_range["start"] ."-". $this->request_range["end"] ."/". $this->size );
                exit;
            }

            $this->request_range = [
                "start" => $start,
                "end" => $end
            ];

            $this->length = $end - $start + 1;
            header('HTTP/1.1 206 Partial Content');
            header("Content-Range: bytes ". $this->request_range["start"] ."-". $this->request_range["end"] ."/". $this->size );

            $this->length = $this->request_range["end"] - $this->request_range["start"];
        }
    }
}