# tools

## routeAutoLoadCheck
this method will final check for the current request so if it's a file request or it's an auto route request;
this method can be enabled or disabled using :
```php
\Config::set( "autoload_controller" , true/false );
```
files will be check in each `/f` (as it has been set in conf) directory of each plugin. the directory name can be changed by
```php
\Config::set( "plugin_file_dir", <dirNameInString>);
```

### auto route loader for non files
there must be a class name `<pluginName>routes.php` with NO namespace. there must be a class inside with the name `<pluginName>Routes` with the uppercase R as below
```php
class userRoutes 
{
    ....
}
```

then goes the route callbacks. for each callback there must be a `public static` function and named over route. for example
the route `/user/add` needs structure below.
```php
// file name is userroutes.php in src directory
class userRoutes
{
    public static function add($request, $response)
    {
        .....
    }
}
```
to separate the request method for example `post` method the function name must finish with `_post`. example :
```php
class userRoutes
{
    public static function add_post($request, $response)
    {
        .....
    }

    public static function add_get($request, $response)
    {
        .....
    }
    
}
```