<?php
namespace redis;

class Connection {
    public $scheme = 'tcp';
    public $host = '127.0.0.1';
    public $port = 6379;
    public $database = 0;
    public $read_write_timeout = 60;
}