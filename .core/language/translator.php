<?PHP
namespace language;

class Translator extends \templating\Templating
{
    private $languageList = [];
    private $language_translate_list = [];

    public function render(string $content)
    {
        $translateList = $this->translateList();
        foreach ($translateList as $masterRegex => $subRegexList) {
            if (!preg_match_all($masterRegex, $content, $matchList))
            return $content;
            foreach ($matchList[1] as $index => $item) {
                $translation = $this->translate($item, $subRegexList);
                $content = str_replace($matchList[0][$index], $translation, $content);
            }
        }
        return $content;
    }

    public function add(string $languageName, string $languageFile)
    {
        $this->languageList[$languageName] = $languageFile;
        return $this;
    }

    public function load(string $languageName)
    {
        $file = $this->languageList[$languageName] ?? false;
        if(!$file)
            \Tool::error("language (" . $languageName . ") has not been set!");
        $filePath = \Tool::cookFileAdress($file, "file");
        if (!file_exists($filePath))
            \Tool::error("language (" . $languageName . ") json file with the directory " . $filePath . " Not found!" );
        $data = json_decode(file_get_contents($filePath), true);
        if(is_null($data))
            \Tool::error("invalid json file " . $filePath);
        $this->language_translate_list = $data;
        return $this;
    }

    public function translateList()
    {
        return $this->language_translate_list;    
    }
}
