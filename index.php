<?php
/**
 * verison 1 : stable
 */

define('prod', true);
define('APACHE', strpos(strtolower($_SERVER['SERVER_SOFTWARE'] ?? ''), 'apache') !== false);
define('CLI', php_sapi_name() === 'cli');

define("SOURCE_DIR", $_SERVER['FW_SOURCE_DIR'] ?? die('NO SOURCE DIR CONFIGURED'));
define("FW_DIR", $_SERVER['FW_FW_DIR'] ?? die("NO FW DIR CONFIGED"));
define("APP_INDEX", $_SERVER['FW_APP_INDEX'] ?? die("NO APP INDEX DIR CONFIGED"));

define("CONFIGFILE_DIR", $_SERVER['FW_CONFIGFILE_DIR'] ?? die("NO CONFIGFILE DIR CONFIGED"));

include 'autoloader.php';

// project defines
define("__url", \Config::get("url"));
if ($timezone = \Config::get("timezone")) {
    date_default_timezone_set($timezone);
}

// start OB and session to rull all over the project
ob_start();
if (CLI) {
    session_id('CLICLIENT');
}
session_start();

// set default route
$target = SOURCE_DIR . APP_INDEX;
$target_exists = file_exists($target);
if (!$target || !$target_exists) {
    die('hello world~!');
} else {
    include $target;
}

// respond to the current request.
// if route not available then call the file autoloader check
\Route::start("\\Tool::routeAutoLoadCheck");