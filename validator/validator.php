<?php
namespace validator;

class Validator
{
    protected $type = 'filter';

    public static function check($value)
    {
        $that = new static();
        return $that->_check($value);
    }

    public function _check($value)
    {
        if ($this->type === 'filter') {
            return filter_var($value, $this->pattern) ? true : false;
        } elseif ($this->type === 'regex') {
            return preg_match($this->pattern, $value) ? true : false;
        } elseif ($this->type === 'function') {
            return $this->validator($value);
        }
    }
}
