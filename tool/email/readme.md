# Elastic email
elastic email API is only to send email through elasticemail.com.

# requirements
you need to have a valid API key from elasticemail.com

# methods
parameters in {this} are optional;

| method | parameters | description |
| --- | --- | --- |
| ->from | email, {name} | set the email that receiver will see and reply to |
| ->to | email | the target email address |
| ->send | title, content, {isHtml:false} | the email body goes here and it will send the email through API |

# send email
to send email first you need to create object from elasticEmail. then you set the `from` email and then
the email you want to send the mail to. 
```php

$email = new \tool\email\ElasticEmail();
$email
    ->from(<your email>, <your name>)
    ->to(<target email>)
    ->send(<mail title>, <content>, <send as html or not>)