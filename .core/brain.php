<?php

use tool\FileExplorer;

// autoloader main configurations
define("CORE_DIR", "core");
define("SOURCE_DIR", "/app");

// configure classes autoloader
\spl_autoload_register(function ($calledClass){
	// cook class name to a knowable class name
	$className = ltrim($calledClass, '\\');
	/* 
		check for it in src if it's not there then check in core
	*/

	// namespace rto directory of target file
	$lastNamespace = "";
	if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $lastNamespace  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
	}
	$file =  strtolower($lastNamespace . $className . '.php');
	$mainDirList = [SOURCE_DIR, CORE_DIR];
	foreach ($mainDirList as $mainDir) {
		$mainDir = $mainDir . DIRECTORY_SEPARATOR;
		if (file_exists($mainDir . $file))
			require_once $mainDir . $file;
		if((!file_exists($mainDir . $file) || !class_exists($calledClass)) && $lastNamespace == "") {
			$secondaryFileDirectory = strtolower($mainDir . $lastNamespace . $className . DIRECTORY_SEPARATOR . $className . ".php");
			if (file_exists($secondaryFileDirectory))
				require_once $secondaryFileDirectory;
		}
		if (class_exists($calledClass)) {
			break;
		}
	}
});

