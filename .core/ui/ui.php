<?php

class UI extends \globalstore\Globalstore
{
	public static function add($name)
	{
		self::set($name, new \ui\UserInterface());
		return self::get($name);
	}

	public static function clone($name, $cloneName)
	{
		self::set($cloneName, clone self::get($name));
		return self::get($cloneName);
	}
}
