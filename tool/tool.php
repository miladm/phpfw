<?php
define("AUTOLOAD_CONTROLLER", \Config::get("autoload_controller") ?: false);

class Tool
{
    public static function config_polish()
    {
        $paramters_wit_dir = [
            "url"
        ];
        foreach($paramters_wit_dir as $parameter) {
            if (!$direction = \Config::get($parameter))
                continue;
            if ($direction[ strlen($direction) - 1 ] != "/")
                \Config::set($parameter, $direction."/");
        }

    }

    /*
        - redirect url
    */
    public static function red($url)
    {
        if ($url[0] == '/')
            $url = substr($url, 1);
        header("location:". __url. $url);
        die();
    }

    /*
        -SCRIPTERROR is showing error for the developer if somewhere went WRONG
    */
    public static function error($value)
    {
        ob_clean();
        die(trigger_error("SCRIPT ERROR : ". $value));
    }

    /**
     * load and pront file
     */

    // public static function printFile_response($request, $response, $filePath)
    // {
    //     printFile($filePath);
    // }

    public static function printFile(string $filePath)
    {
        $file = new \tool\FileExplorer($filePath);
        $file->print();
    }

    /**
     * check if the route request file exists then respond with the file
    */
    public static function routeAutoLoadCheck($requestString, $requestData, $callback, $method)
    {
        $main = substr($requestString, 1, strpos($requestString, "/", 1) - 1);
        $rest = substr($requestString, strlen($main) + 1 + 1, -1);
        if (strpos($requestString, ".") !== false) {
            
            /* 
                the rest of the directory will be the maindir + two slashes that we not detected
                /maindir/myfile.css/ ==> 7(maindir) + 1(/) + 1(/) : output => myfile.css/
                then we remove the last / from the file name
             */
            $fileDir = \config::get("plugin_file_dir") ?? "f";
            $file = SOURCE_DIR . "/" . $main . "/" . $fileDir . "/" . $rest;
            if (!file_exists($file))
                return false;
            self::printFile($file);
        } elseif (/* strpos($rest, "/") === false && */ AUTOLOAD_CONTROLLER) {
            
            /*
                here we are going todo auto route load if available
                first we have to get the first two request paramters as the 
                class/method and the rest will be passed as parameters
            */
            $main = ucfirst($main);
            if (strpos($rest, '/') === false) {
                $seccond = $rest;
                $rest = false;
            } else {
                $seccond = substr($rest, 0, strpos($rest, "/"));
                $rest = substr($requestString, strlen($seccond) + strlen($main) + 1 + 1, -1);
            }
            
            if (!$seccond)
                return false;
            if ($rest) {
                preg_match_all('/\/([^\/]*)/', $rest, $matchList);
                $requestData->param = (object) $matchList[1];
            }
            $targetControllerMethod = '\\' . $main . 'Routes::' . $seccond;
            $method = strtolower($method);
            if (is_callable($targetControllerMethod . '_' . $method)) {
                $callback->setCallback($targetControllerMethod . '_' . $method)->run($requestData);
            } elseif (is_callable($targetControllerMethod)) {
                $callback->setCallback($targetControllerMethod)->run($requestData);
            }
        }
        return false;
    }

    public static function force_file_put_contents($file, $data)
    {
        if(!$file)
            \Tool::error("file name can't be null");
        if ($file[0] == "/")
            $file = substr($file, 1);
        $dirList = explode("/", $file);

        // get rid of file name
        array_pop($dirList);
        $path = "";
        foreach ($dirList as $dir) {
            if (!is_dir($path . $dir)) {
                mkdir($path . $dir);
                chmod($path . $dir, 0777); 
            }
            $path .= $dir . "/";
        }
        file_put_contents($file, $data);
        chmod($file, 0777); 
    }

    public static function cookFileAdress($file, $fileType)
    {
        switch ($fileType) {
            case "template":
                $base = \Config::get("plugin_template_dir") ?: "t";
                break;
            case "file":
                $base = \Config::get("plugin_file_dir") ?: "f";
                break;
                case "interface":
                $base = \Config::get("plugin_template_dir") ?: "i";
                break;
            case "view":
                $base = \Config::get("view_dir") ?: "views";
                break;
            default:
                $base = "";
                break;
        }
        if ($fileType == 'view') {
            $file = $base . '/' . $file;
        } else {
            if ($base[0] != '/')
                $base = '/' . $base;
            if (strpos($file, "@")) {
                $exfile = explode("@", $file);
                $file = $exfile[1] . $base . "/" . $exfile[0];
            }
        }
        return SOURCE_DIR . "/" . $file;
    }

    public static function cookFileUrl($fileAddress)
    {

        /*
            if it's a url file just return the address
        */
        if(preg_match('/^http(?:s|):\/\//', $fileAddress)) 
            return $fileAddress;

        /*
            else go fot the file in project
        */
        if (strpos($fileAddress, "@")) {
            list($restOfAddress, $pluginName) = explode("@", $fileAddress);
            $siteUrl = \Config::get("url");
            if(!$siteUrl)
                \Tool::error("you must config URL in \Config");
            $fileUrl = $siteUrl . $pluginName . "/" . $restOfAddress;
        }
        return $fileUrl ?? __url.$fileAddress;
    }

    public static function email($to, $title, $message, $htmlHeader = false, $from = false, $replyTo = false)
	{
		$e = new \tool\Email();
        if ($from) 
            $e->from($from);
        if ($replyTo) 
            $e->replyTo($replyTo);
        if ($htmlHeader === true) 
            $e->htmlHeader();
		elseif ($htmlHeader)
			$e->htmlHeader($htmlHeader);
		return $e->message($title, $message)->to($to)->send();
    }
    
    public static function buildKey($length = 5, $charset = false)
	{
		$string = "";
		$characters = $charset?:"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        while ($length--) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }

        return $string;
    }
    
    public static function hash($string)
    {
        return md5($string . \Config::get('hash_id'));
    }

    public static function getUserIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function checkCLI()
    {
        return (php_sapi_name() === 'cli');
    }
}
