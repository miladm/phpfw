
# add index to auto loader
to make a directory as the index of routing for auto loader user the method below in the `app.php` (on any index file that is loaded) :
```php
class ComponentAutoLoader extends AutoLoader
{
	static $indexList = ['/app/component']; // <-- must be in array
}
\spl_autoload_register('ComponentAutoLoader::load'); // <-- here it registers the auto loader
```




    # create route

    ## Route method
    add route to `app.php` or any file that has been loaded in the framework
    ```php
    \Route::get()
    ```
    full documentation on [route readme file](./route/readme.md)

    ## Controller method
    create a file with the name of controller in `src` directory and name it after controller(first letter must be uppercase)

    ### setup
    `autoload_controller` must be set as `true` anywhere int the application.(better to be set on `set/configs.php`)
    ```php
    \Config::set("autoload_controller", true);
    ```

    example : post conttoller
    * create file `src/post.php`
    * add class below
    * add target route of current controller as a `public static function`
    ```php
    class Post
    {
        public static function new ($request, $response)
        {
            $response->send("hello world");
        }
    }
    ```
    now if you open `http://site/post/new` this method will be called

        **Note:** the route method will comes first then the controller method comes if there's no route for current request.