<?php

$globalTranslator = null;

class Language
{
    public static function add(string $languageName, string $file)
    {
        return self::getGlobalVariable()->add($languageName, $file);
    }

    public static function load(string $languageName)
    {
        return self::getGlobalVariable()->load($languageName);
    }

    private static function getGlobalVariable()
    {
        global $globalTranslator;
        if (is_null($globalTranslator))
            $globalTranslator = new \language\Translator;
        return $globalTranslator;
    }
}