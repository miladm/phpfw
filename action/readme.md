[Home](../readme.md)

	this documentation must be updated


# Actions
Each plugin has its' own task. sometimes some part of the plugin has to communicate and interact with other plugins.

Action hook is here to setup a position for unknown future actions and interactions. 

Ex. you know in the future there will be things to happen after registration so you label the registration just in the place that the registration success, and it will be the possibility to setup action anywhere and anytime in the future.

You can setup and do actions `before` and `after` so this module has both before setup and after setup and before action and after action.

**NOTE :**Actions must be added to the main file that is loading as the primary application. if you use the actions as a autoload method it will not work.

## functions
| name | description |
| --- | --- |
| `action::add_before( $actionName, $function )` | to setup action before |
| `action::add_after( $actionName, $function )` | to setup action after |
| `action::do_after( $actionName, $data )` | to do the action before. calling the list of actions one by one |
| `action::do_after( $actionName, $data )` | to do the action after. calling the list of actions one by one |

### `$actionName`
A unique name for the current action.

### `$function`
The function name or the function pointer (variable) to be called on the event.

### `$data` 
List of data we wanna pass the function in case they need it.


## Example


```php

function login( $username, $password )
{
	action::do_before( "login", $username );

	if( verify( $username, $password ))
	{
		.....
	}
	
	action::do_after( "login", $username )
	return true;
}

.....
//somewhere on other plugins
action::add_after("login", function( $username ){
	email( "wellcome ".$username );
});


```