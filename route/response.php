<?php

namespace route;

define("TEMPLATING_ENGINE", \Config::get("templating_engine") ?: "default");

class Response
{
    private $callback = null;
    private $useList = [];
    private $thenList = [];
    private $parameters = null;
    private $request = null;
    private $headerCode = 0;
    private $tag = null;

    function __construct(callable $callback = null, $parameters = false)
    {
        $this->callback = $callback;
        $this->parameters = $parameters;
    }

    public function use(callable $use, $variables = 'SomeRandomParameterThatWillNotEverBeenSet7842364783428jhf')
    {
        if ($variables == 'SomeRandomParameterThatWillNotEverBeenSet7842364783428jhf')
            $this->useList[] = [$use];
        else
            $this->useList[] = [$use, $variables];
        return $this;
    }

    public function then(callable $use, $variables = 'SomeRandomParameterThatWillNotEverBeenSet7842364783428jhf')
    {
        if ($variables == 'SomeRandomParameterThatWillNotEverBeenSet7842364783428jhf')
            $this->thenList[] = [$use];
        else
            $this->thenList[] = [$use, $variables];
        return $this;
    }

    public function tag(string $tag)
    {
        $this->tag = $tag;
        return $this;
    }

    public function getTag()
    {
        return is_null($this->tag) ? false : $this->tag;
    }

    public function setCallback(callable $callback, $parameters = false): Response
    {
        $this->callback = $callback;
        $this->parameters = $parameters;
        return $this;
    }

    public function callable()
    {
        return $this->callback != null;
    }

    public function run(\route\Request $request)
    {
        $this->request = $request;
        $that = clone $this;
        if ($nextCall = $that->getNextCall()) {
            $nextCall = (array) $nextCall;
            if (isset($nextCall[1]))
                $nextCall[0]($request, $that, $nextCall[1]);
            else
                $nextCall[0]($request, $that);
        }
    }

    public function runThen($responseBody)
    {
        while (($thenCall = $this->getThenCall()) != false) {
            $thenCall = (array) $thenCall;
            if (isset($thenCall[1]))
                $responseBody = $thenCall[0]($responseBody, $thenCall[1]);
            else
                $responseBody = $thenCall[0]($responseBody);
        }
        return true;
    }

    public function next($data = [])
    {
        if (!empty($data))
            $this->request->attach($data);
        if ($this->request === null)
            $this->request = new \route\Request;
        $this->run($this->request);
    }

    public function getNextCall()
    {
        $nextCall = \array_shift($this->useList);
        if (\is_null($nextCall)) {
            if (is_null($this->parameters)) {
                return [$this->callback];
            } else {
                return [$this->callback, $this->parameters];
            }
        } else {
            return $nextCall;
        }
    }

    public function getThenCall()
    {
        $nextCall = \array_shift($this->thenList);
        if (\is_null($nextCall))
            return false;
        else
            return $nextCall;
    }

    public function status(int $headerCode): Response
    {
        $this->headerCode = $headerCode;
        return $this;
    }

    public function redirect($url)
    {
        ob_clean();
        if ($this->headerCode === 0)
            $this->headerCode = 302;
        $this->sendHeader();
        header('Location : ' . __url . $url);
    }

    public function send(string $string)
    {
        $this->runThen($string);
        ob_clean();
        // required header for utf-8 websites
        Header('content-type:  text/html; charset=utf-8');
        $this->sendHeader();
        die($string);
    }

    public function ui($uiname)
    {
        $ui = \UI::get($uiname);
        if (!$ui)
            \Tool::error("the UI with the name $uiname not exists");
        return $ui;
    }

    public function file($file)
    {
        $this->runThen($file);
        $file = \Tool::cookFileAdress($file, "file");
        if (!file_exists($file))
            \Tool::error("the file $file not found");
        ob_clean();
        \Tool::printFile($file);
    }

    public function render($fileToBeRendered)
    {
        $this->runThen($fileToBeRendered);
        // required header for utf-8 websites
        Header('content-type:  text/html; charset=utf-8');
        $file = \Tool::cookFileAdress($fileToBeRendered, "template");
        if (!file_exists($file))
            \Tool::error("file $file not found to be rendered!");
        ob_clean();
        if (TEMPLATING_ENGINE == "default") {
            echo file_get_contents($file);
        }
        die();
    }
    /**
     *  
     */
    public function json($array, $header = 200)
    {
        $this->runThen($array);
        ob_clean();
        $this->sendHeader();
        header('Content-Type: application/json');
        $header = $this->headerCode2String($header);
        header($header);
        echo json_encode($array);
        die();
    }

    public function error404()
    {
        $this->error("HTTP/1.0 404 Not Found", 404);
    }


    public function error400()
    {
        $this->error('HTTP/1.1 400 Bad Request', 400);
    }

    public function error($header, $code)
    {
        ob_clean();
        header($header, true, $code);
        die();
    }


    private function sendHeader()
    {
        if ($this->headerCode > 0) {
            if ($headerString = $this->headerCode2String($this->headerCode)) {
                header($headerString, true, $this->headerCode);
            }
        }
    }

    private function headerCode2String($code)
    {
        return \HTTP\Status::code2string($code);
    }
}
