<?php
namespace prototype;

class Validate
{
    public static function check($value, $type)
    {
        switch ($type) {
            case 'int':
            case 'bigInt':
                $validationResult = \validator\Integer::check($value);
                break;

            case 'email':
                $validationResult = \validator\Email::check($value);
                break;

            case 'url':
                $validationResult = \validator\URL::check($value);
                break;

            case 'string':
            case 'hash':
            case 'text':
            case 'timestamp':
            case 'JSON':
                $validationResult = true;
                break;

            case 'boolean':
                $validationResult = \validator\Boolean::check($value);
                break;

            case 'object':
                $validationResult = self::validateObject($value);
                break;
        }

        if (!$validationResult) {
            return self::error('validate', "the value '$value' for $fieldName must be $type");
        }
        return true;
    }

    private static function validateObject($object)
    {
        // if the id of the object passed
        if (is_int($object)) {
            return true;
        }

        // if the id of the object passed as string
        if (is_string($object)) {
            if (preg_match('/^\d*$/', $object)) {
                return true;
            }
        }
        // if the object passed
        if (is_object($object) &&
            // get_class($object) == 'prototype\SchemaTable' && 
        isset($object->id) &&
            is_int($object->id)) {
            return true;
        }

        return false;
    }

    private static function error($paramter, $value)
    {
        \_E::set($paramter, $value);
        return false;
    }
}