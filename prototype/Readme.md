[Home](../readme.md)

# Prototype introduction

prototype is a pre built structure for table structures based on three tire architecture.
this component is based on [table](../database/readme.md) so if you wanna develop on prototype make sure you have read the [table documentation](../database/readme.md).

# prototype initiate
to create a new prototype you have to create an application layer as below:
```PHP
class my_prototype extends \Prototype
{
    protected function init()
    {
        //some initiates here
    }

    // custom methods goes here

}

my_prototype::start();
```
the ```init()``` function is all configurations and constructions you need to do on object creation event. there's no need to create ```__construct``` on prototype.

the ```::start()``` is to run and register the prototype in the system. it will ```return``` an obejct of current prototype so it can be used anywhere.

next you have to put the database schema on ```init``` function so the prototype understands the database structure. **schema documentation is on data-access layer documentation below***
```PHP
class my_prototype extends \Prototype
{
    protected function init()
    {
        $this->schema( "my_table_name" )
            ->int("phoneNumber")
            ->string( "name" )
            ->private_string( "password" );
    }
    // custom methods goes here
}
```
Important methods are prebuilt in prototype but if you need more application layer methods you can add this class
```PHP
class my_prototype extends \Prototype
{
    protected function init()
    {
        $this->schema( "my_table_name" )
            ->int("phoneNumber")
            ->string( "name" )
            ->private_string( "password" );
    }

    public function check_login( $username, $pass )
    {
        //things here
    }

    private function hash_password( $password )
    {
        // things here
    }
}
```

## interface layer
Interface methods are static so you can call them anywhere.
```PHP
my_prototype::getLast( ["id=?", 12] );
my_prototype::check_login(  );
```

## data-access layer
This layer will be available fast and easy. its only requirement is the schema of the database and tables.

### database schema
data schema is based on database table for current prototype. it should define each fields with the attributes below :
* table name
* type of field (each)
    * int
    * string
    * text
    * boolean
    * timestamp [not available in current version]
    * object*
    * json
* accessability
    * public
    * private
* table primary key
* relations with other tables

[ * ] object is the type that will load another schema in itself

#### create schema
Schema has been initiate in prototype. to configure schema you have to call ```schema()``` method;
```php
$this->schema( "my_schema_name" );//this will return schema object
```
to add field to schema you can use this reference :

**NOTE:** there are more configurations for each method like the length of field. you can check the ```schema.php``` file for more details.

| method name | type | action |
| ---   | ---   | --- |
| `->int( "field_name" )` | public | creates an integer field with the name "field_name" |
| `->private_int( "field_name" )` | private | creates an integer field with the name "field_name" |
| `->string( "field_name" )` | public | creates an string (varchar with the length 200 ) field with the name "field_name" |
| `->private_string( "field_name" )` | private | creates an string (varchar with the length 200 ) field with the name "field_name" |
| `->text( "field_name" )` | public | creates an text field with the name "field_name" |
| `->private_text( "field_name" )` | private | creates an text field with the name "field_name" |
| `->boolean( "field_name" )` | public | creates an boolean field with the name "field_name" |
| `->private_boolean( "field_name" )` | private | creates an boolean field with the name "field_name" |
| `->object( "field_name", "schema_name )` | public | creates an object field with the name "field_name". the insider shcema of this object will be loaded as the table schema_name's shema  |
| `->private_object( "field_name", "schema_name )` | private | creates an object field with the name "field_name". the insider shcema of this object will be loaded as the table schema_name's shema  |
| `->email( "field_name" )` | public | creates a type os string with validation of email |
| `->url( "field_name" )` | public | creates a type os string with validation of url |
| `->hash( "field_name" )` | public | creates a type os string with a hashed value |
| `->json( "field_name" )` | public | stores objects in the form of json into text field |
| `->many(<name>, <schema name>, <relation Schema Name = false>, <nameOnSchema = false>)` | public | create one to many and many to many relations |

##### actions on current field
| method | action |
| ---- | ---- |
| `->private()` | make last added field a private field |
| `->unique()` | make last added field a unique field |
| `->notNull()` | make last added field a not null field |
| `->length(<int>)` | make last added field's length to integer value |
| `->default(<value>)` | make last added field's default value as the entered value |

## application layer
prototype has some prebuilt application layer methods

_parametes with star (*) are required_

| method name | parameters | action |
| ---- | ---- | ---- |
| create() | none | this will create this prototype table if necessary |
| ::add() | [data*] | this will add the data to the database |
| ::update () | [data*, condition*] | this will update data on the entered condition |
| ::map() | [array of mapping] | map and structure the output |
| ::groupBy() | $columnName(string) | prepare query to be grouped by specific column |
| ::where() | [condition] | // prepare query with special condition that other methods doesn't support. **NOTE**:  `this method is ready but not tested so it has been commented` |
| `::orderBy()` | -- | `idea of adding orderBy method to prototype`|
| ::get() | [condition, max_return_fields(limit)(int), start_pointer(int), order_asc(boolean) ] | this will return an array of data from database |
| ::getOne() | [condition, order_asc(boolean)] | returns only one result |
| ::getFirst() | [condition] | returns only one result |
| ::getLast() | [condition] | returns only one result |
| ::count() | [condition] | returns count of results |
| ::expect() | [expectedNumber = 1, condition] | check if the count of result is as entered number and will return boolean |

### Condition parameters' structure
Condition must be in array style. first parameter is the condition and the second is data (as ```where``` method in _table_)
```PHP
prototype::get( [ "user.id=?", 12 ] )
```
```PHP
prototype::get( [ "user.id=?&user.name=?", [12, "admin"] ] )
```
### add new method to application layer
**best solution**
If the ```get``` method is enough for you and you can custom your method using that it's better to go through ```get```
```PHP
public function anouther_selection( $parameter )
{
    $condition = $parameter;
    return self::get( $condition );
}
```
**otherwise**

You can easily add methods to your prototype just keep in mind to access table object you have to call ```$this->table``` variable of the prototype object.

other important thing is the selection list. as there are some private fields in the table you have to select as the ```$this->schema()->selectList()``` say. you can see example as below:
```php
public function anouther_selection( $someParameters )
{
    $selectList = $this->schema()->selectList();
    return $this->table->.........->select( $selectList )
}
```

#### mapping
to have a customized structure for output you can set the map before any `get` method
```php
\prototype::map(['otherName' => 'name', ... ])->get(...);
```

#### mapGroup
**Note:** only works after mapping.

in some cases there are many repeated rows with only one different field. example:
| name | age | food |
| --- | --- | --- |
| danny | 12 | apple |
| danny | 12 | chicken |
| danny | 12 | fries |
| danny | 12 | pie |

there is possibility to group all similar data and get different values as an array like below.
```javascript
{
    name : 'danny',
    age : 12,
    foodList: [
        {'foodName' : 'apple'}, 
        {'foodName' : 'chicken'}, 
        {'foodName' : 'fries'}, 
        {'foodName' : 'pie'}
    ]
}
```
all has to be done is to set mapping and `groupBy` after it as below:
```php
\foodTableName::map([
    'name' => 'user.name',
    'age' => 'user.age',
    'foodList' => [
        'foodName' => 'food'
    ]
])->mapGroup(`name`)->get(...);
```

### relations
here by __table__ we mean __prototype__ and actually there are no difference. prototypes are dataaccess layer + some main controller methods.
#### one to one
just we set the sub table as `object`
```php
$this->schema('profile')
    ->string('name')
    ->object('owner', 'user');
```
**NOTE:** object schema name can be in namespace
```php
$this->schema('profile')
    ->string('name')
    ->object('owner', '\app\user');
```
    #### one to many
    add the sub table to main tbale with `many` and add main table as `object` to sub table;
    ```php
    $this->schema('page')
        ->string('name')
        ->many('postList', 'post');// or ->many('post')

    $this->schema('post')
        ->string('title')
        ->text('content')
        ->object('page', 'page');
    ```
    **NOTE:** many schema name can be in namespace
    ```php
    $this->schema('page')
        ->string('name')
        ->many('postList', '\app\user\post');// or ->many('post')
    ```
    #### many to many
    there must be a relation-table to make many to many connection.
    * create relation prototype(table)
    * put both side objects to make connections
    * set a `many` to first prototype
    * set a `many` to second prototype
    ```php
    $this->schema('book')
        ->string('name')
        ->many('author', 'author');// or ->many('author')

    $this->schema('author')
        ->string('name')
        ->many('book', 'book');// or ->many('book')

    $this->schema('bookAuthorRelation')
        ->object('author', 'author')
        ->object('book', 'book')
    ```

    # tag
    tag will be assigned to a route so it will be used on roles and permissions .
    ```php
    \Route::get(.....)->tag('normalAccess');
    ```
    ## use case
    ```php
    \Route::use(function($req, $res) {
        $tag = $res->getTag();

        if ($user->premission[$tag] ?? false) { // if isset and it's true
            $res->next();
        }
        $res->status(401)->send('you are not authorized');
    })
    ```

# Updating plan
* search system with exploding on space characters
* set required inputs in schema (notnull and check on inputs on adding new)
